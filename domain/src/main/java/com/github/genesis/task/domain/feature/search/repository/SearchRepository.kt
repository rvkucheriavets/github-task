package com.github.genesis.task.domain.feature.search.repository

import android.arch.paging.PagedList
import com.github.genesis.task.domain.feature.search.model.Repository
import com.github.genesis.task.domain.feature.search.model.SearchQuery
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface SearchRepository {

    fun searchForRepositories(query: SearchQuery): Flowable<PagedList<Repository>>

    fun getCachedSearchQueries(): Single<List<SearchQuery>>

    fun updateRepositoryViewedState(repositoryId: Long, isViewed: Boolean): Completable
}
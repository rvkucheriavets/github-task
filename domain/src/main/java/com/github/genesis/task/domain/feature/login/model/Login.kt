package com.github.genesis.task.domain.feature.login.model


data class Login(
    val login: String,
    val password: String
)
package com.github.genesis.task.domain.feature.search.model

data class Owner(
    val receivedEventsUrl: String,
    val avatarUrl: String,
    val id: Long,
    val login: String,
    val type: String,
    val gravatarId: String,
    val url: String,
    val nodeId: String
)
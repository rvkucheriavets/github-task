package com.github.genesis.task.domain.feature.search.usecase

import com.github.genesis.task.domain.executor.PostExecutionThread
import com.github.genesis.task.domain.executor.ThreadExecutor
import com.github.genesis.task.domain.feature.search.repository.SearchRepository
import com.github.genesis.task.domain.interactor.CompletableInteractor
import io.reactivex.Completable
import javax.inject.Inject

class UpdateViewedStateUseCase @Inject constructor(
    private val searchRepository: SearchRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : CompletableInteractor<Pair<Long, Boolean>>(threadExecutor, postExecutionThread) {

    override fun buildUseCaseObservable(params: Pair<Long, Boolean>): Completable =
        searchRepository.updateRepositoryViewedState(params.first, params.second)
}
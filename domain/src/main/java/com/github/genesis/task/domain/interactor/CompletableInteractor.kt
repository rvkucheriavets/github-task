package com.github.genesis.task.domain.interactor

import com.github.genesis.task.domain.executor.PostExecutionThread
import com.github.genesis.task.domain.executor.ThreadExecutor
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers

/**
 * Abstract class for a UseCase that returns an instance of a [Completable].
 */
abstract class CompletableInteractor<in Params> constructor(
        private val threadExecutor: ThreadExecutor,
        private val postExecutionThread: PostExecutionThread) {

    private val subscription = CompositeDisposable()

    /**
     * Builds a [Completable] which will be used when the current [CompletableInteractor] is executed.
     */
    protected abstract fun buildUseCaseObservable(params: Params): Completable

    /**
     * Executes the current use case.
     */
    open fun execute(params: Params, observer: CompletableObserver? = null): Completable {
        val observable = this.buildUseCaseObservable(params)
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.scheduler)
                .doOnSubscribe { it.addTo(subscription) }

        observer?.let { observable.subscribeWith(it) }
        return observable
    }

    /**
     * Unsubscribes from current [Disposable].
     */
    fun unsubscribe() {
        if (!subscription.isDisposed) {
            subscription.dispose()
        }
    }

}
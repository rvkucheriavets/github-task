package com.github.genesis.task.domain.feature.search.usecase

import com.github.genesis.task.domain.executor.PostExecutionThread
import com.github.genesis.task.domain.executor.ThreadExecutor
import com.github.genesis.task.domain.feature.search.model.SearchQuery
import com.github.genesis.task.domain.feature.search.repository.SearchRepository
import com.github.genesis.task.domain.interactor.SingleInteractor
import io.reactivex.Single
import javax.inject.Inject

class LatestSearchQueriesUseCase @Inject constructor(
    private val searchRepository: SearchRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : SingleInteractor<List<SearchQuery>, Unit>(threadExecutor, postExecutionThread) {

    override fun buildUseCaseObservable(params: Unit?): Single<List<SearchQuery>> =
        searchRepository.getCachedSearchQueries()
}
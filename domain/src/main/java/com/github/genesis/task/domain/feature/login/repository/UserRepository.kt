package com.github.genesis.task.domain.feature.login.repository

import com.github.genesis.task.domain.feature.login.model.Login
import io.reactivex.Observable


interface UserRepository {

    fun login(login: Login): Observable<Any>
}
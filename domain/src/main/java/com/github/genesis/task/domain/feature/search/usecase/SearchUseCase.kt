package com.github.genesis.task.domain.feature.search.usecase

import android.arch.paging.PagedList
import com.github.genesis.task.domain.executor.PostExecutionThread
import com.github.genesis.task.domain.executor.ThreadExecutor
import com.github.genesis.task.domain.feature.search.model.Repository
import com.github.genesis.task.domain.feature.search.model.SearchQuery
import com.github.genesis.task.domain.feature.search.repository.SearchRepository
import com.github.genesis.task.domain.interactor.FlowableInteractor
import io.reactivex.Flowable
import javax.inject.Inject

class SearchUseCase @Inject constructor(
    private val searchRepository: SearchRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : FlowableInteractor<PagedList<Repository>, SearchQuery>(threadExecutor, postExecutionThread) {

    override fun buildUseCaseObservable(params: SearchQuery?): Flowable<PagedList<Repository>> =
        searchRepository.searchForRepositories(checkNotNull(params))
}
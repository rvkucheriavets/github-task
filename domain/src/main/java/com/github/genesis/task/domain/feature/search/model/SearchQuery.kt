package com.github.genesis.task.domain.feature.search.model

data class SearchQuery(
    val query: String,
    val sortType: SortType? = null,
    val sortDirection: SortDirection? = null
)
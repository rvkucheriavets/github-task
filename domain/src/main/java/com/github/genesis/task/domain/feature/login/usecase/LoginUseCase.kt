package com.github.genesis.task.domain.feature.login.usecase

import com.github.genesis.task.domain.executor.PostExecutionThread
import com.github.genesis.task.domain.executor.ThreadExecutor
import com.github.genesis.task.domain.feature.login.model.Login
import com.github.genesis.task.domain.feature.login.repository.UserRepository
import com.github.genesis.task.domain.interactor.ObservableInteractor
import io.reactivex.Observable
import javax.inject.Inject


class LoginUseCase @Inject constructor(
    private val userRepository: UserRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : ObservableInteractor<Any, LoginUseCase.Param>(threadExecutor, postExecutionThread){

    override fun buildUseCaseObservable(params: Param?): Observable<Any> =
        userRepository.login(checkNotNull(params?.login))

    data class Param(
        val login: Login
    )
}
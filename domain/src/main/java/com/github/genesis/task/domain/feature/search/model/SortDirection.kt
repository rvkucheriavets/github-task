package com.github.genesis.task.domain.feature.search.model

enum class SortDirection {
    ASC, DESC
}
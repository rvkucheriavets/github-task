package com.github.genesis.task.data.feature.login.datasource

import com.github.genesis.task.data.feature.login.entity.LoginEntity
import io.reactivex.Observable


interface UserDataSource {

    fun login(loginEntity: LoginEntity): Observable<Any>
}
package com.github.genesis.task.data

interface EntityMapper<E, D> {

    fun mapFromEntity(source: E): D

    fun mapToEntity(source: D): E

    fun mapFromEntity(source: List<E>): List<D> = source.map { mapFromEntity(it) }

    fun mapToEntity(source: List<D>): List<E> = source.map { mapToEntity(it) }
}
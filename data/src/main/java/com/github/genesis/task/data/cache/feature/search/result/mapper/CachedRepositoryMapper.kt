package com.github.genesis.task.data.cache.feature.search.result.mapper

import com.github.genesis.task.data.cache.CacheMapper
import com.github.genesis.task.data.cache.feature.search.result.model.CachedOwner
import com.github.genesis.task.data.cache.feature.search.result.model.CachedRepository
import com.github.genesis.task.data.feature.search.entity.OwnerEntity
import com.github.genesis.task.data.feature.search.entity.RepositoryEntity
import javax.inject.Inject

class CachedRepositoryMapper @Inject constructor(
    private val ownerMapper: CacheMapper<CachedOwner, OwnerEntity>
) : CacheMapper<CachedRepository, RepositoryEntity> {

    override fun mapFromCached(source: CachedRepository): RepositoryEntity =
        with(source) {
            RepositoryEntity(
                ownerEntity = ownerMapper.mapFromCached(checkNotNull(owner)),
                id = id,
                description = description,
                htmlUrl = htmlUrl,
                name = name,
                watchersCount = watchersCount,
                updatedAt = updatedAt,
                stargazersCount = stargazersCount,
                size = size,
                score = score,
                pushedAt = pushedAt,
                openIssuesCount = openIssuesCount,
                masterBranch = masterBranch,
                language = language,
                jsonMemberPrivate = jsonMemberPrivate,
                homepage = homepage,
                fullName = fullName,
                forksCount = forksCount,
                fork = fork,
                defaultBranch = defaultBranch,
                createdAt = createdAt,
                nodeId = nodeId,
                url = url,
                isViewed = isViewed
            )
        }

    override fun mapToCached(source: RepositoryEntity): CachedRepository =
        with(source) {
            CachedRepository(
                id = id,
                description = description,
                htmlUrl = htmlUrl,
                name = name,
                watchersCount = watchersCount,
                updatedAt = updatedAt,
                stargazersCount = stargazersCount,
                size = size,
                score = score,
                pushedAt = pushedAt,
                openIssuesCount = openIssuesCount,
                masterBranch = masterBranch,
                language = language,
                jsonMemberPrivate = jsonMemberPrivate,
                homepage = homepage,
                fullName = fullName,
                forksCount = forksCount,
                fork = fork,
                defaultBranch = defaultBranch,
                createdAt = createdAt,
                nodeId = nodeId,
                url = url,
                ownerId = ownerEntity.id
            )
        }
}
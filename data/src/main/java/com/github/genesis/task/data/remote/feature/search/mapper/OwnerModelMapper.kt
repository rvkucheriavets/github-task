package com.github.genesis.task.data.remote.feature.search.mapper

import com.github.genesis.task.data.feature.search.entity.OwnerEntity
import com.github.genesis.task.data.remote.ModelMapper
import com.github.genesis.task.data.remote.feature.search.model.OwnerModel
import javax.inject.Inject

class OwnerModelMapper @Inject constructor() : ModelMapper<OwnerModel, OwnerEntity> {

    override fun mapFromRemote(source: OwnerModel): OwnerEntity =
        with(source) {
            OwnerEntity(
                receivedEventsUrl = checkNotNull(receivedEventsUrl),
                login = checkNotNull(login),
                id = checkNotNull(id),
                avatarUrl = checkNotNull(avatarUrl),
                gravatarId = checkNotNull(gravatarId),
                nodeId = checkNotNull(nodeId),
                type = checkNotNull(type),
                url = checkNotNull(url)
            )
        }

    override fun mapToRemote(source: OwnerEntity): OwnerModel =
        with(source) {
            OwnerModel(
                receivedEventsUrl = receivedEventsUrl,
                login = login,
                id = id,
                avatarUrl = avatarUrl,
                gravatarId = gravatarId,
                nodeId = nodeId,
                type = type,
                url = url
            )
        }

}
package com.github.genesis.task.data.feature.search.repository

import android.arch.paging.PagedList
import android.arch.paging.RxPagedListBuilder
import com.github.genesis.task.data.EntityMapper
import com.github.genesis.task.data.feature.search.datasource.RepositoriesDataSourceFactory
import com.github.genesis.task.data.feature.search.entity.RepositoryEntity
import com.github.genesis.task.domain.feature.search.model.Repository
import com.github.genesis.task.domain.feature.search.model.SearchQuery
import com.github.genesis.task.domain.feature.search.repository.SearchRepository
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SearchDataRepository @Inject constructor(
    private val repositoriesDataSourceFactory: RepositoriesDataSourceFactory,
    private val repositoryMapper: EntityMapper<RepositoryEntity, Repository>,
    private val disposable: CompositeDisposable
) : SearchRepository {

    override fun searchForRepositories(query: SearchQuery): Flowable<PagedList<Repository>> {
        val pageConfigurations = PagedList.Config.Builder()
            .setPageSize(30)
            .setInitialLoadSizeHint(45)
            .setPrefetchDistance(10)
            .setEnablePlaceholders(false)
            .build()

        return RxPagedListBuilder(repositoriesDataSourceFactory
            .mapByPage { saveResults(query, it); repositoryMapper.mapFromEntity(it) }, pageConfigurations)
            .setInitialLoadKey(query)
            .buildFlowable(BackpressureStrategy.BUFFER)
            .observeOn(Schedulers.io())
    }

    override fun getCachedSearchQueries(): Single<List<SearchQuery>> =
        repositoriesDataSourceFactory.getCacheDataStore().getLatestSearchQueries()

    override fun updateRepositoryViewedState(repositoryId: Long, isViewed: Boolean): Completable =
        repositoriesDataSourceFactory.getCacheDataStore().updateRepositoryViewedState(repositoryId, isViewed)

    private fun saveResults(query: SearchQuery, entities: List<RepositoryEntity>) {
        if (!repositoriesDataSourceFactory.isCurrentDataSourceCache()) {
            repositoriesDataSourceFactory.getCacheDataStore()
                .saveSearchQuery(query)
                .andThen(repositoriesDataSourceFactory.getCacheDataStore().saveSearchResultsWithQuery(entities, query))
                .subscribe()
                .addTo(disposable)
        }
    }
}
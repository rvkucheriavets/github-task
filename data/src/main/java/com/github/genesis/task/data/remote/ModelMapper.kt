package com.github.genesis.task.data.remote

interface ModelMapper<M, E> {

    fun mapFromRemote(source: M): E

    fun mapToRemote(source: E): M

    fun mapFromRemote(source: List<M>): List<E> = source.map { mapFromRemote(it) }

    fun mapToRemote(source: List<E>): List<M> = source.map { mapToRemote(it) }
}
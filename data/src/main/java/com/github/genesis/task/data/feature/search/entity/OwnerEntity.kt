package com.github.genesis.task.data.feature.search.entity

data class OwnerEntity(
    val receivedEventsUrl: String,
    val avatarUrl: String,
    val id: Long,
    val login: String,
    val type: String,
    val gravatarId: String,
    val url: String,
    val nodeId: String
)
package com.github.genesis.task.data.feature.search.datasource

import com.github.genesis.task.domain.feature.search.model.SearchQuery
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface SearchCache {

    fun addNewSearchQuery(query: SearchQuery): Completable

    fun getSearchQueries(): Single<List<SearchQuery>>
}
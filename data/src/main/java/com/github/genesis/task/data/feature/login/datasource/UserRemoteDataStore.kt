package com.github.genesis.task.data.feature.login.datasource

import com.github.genesis.task.data.feature.login.entity.LoginEntity
import io.reactivex.Observable
import javax.inject.Inject


class UserRemoteDataStore @Inject constructor(
    private val loginRemote: LoginRemote
) : UserDataSource {

    override fun login(loginEntity: LoginEntity): Observable<Any> =
        loginRemote.login(loginEntity)
}
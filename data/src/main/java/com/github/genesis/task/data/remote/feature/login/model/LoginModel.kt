package com.github.genesis.task.data.remote.feature.login.model


data class LoginModel(
    val login: String,
    val password: String
)
package com.github.genesis.task.data.remote.feature.login

import com.github.genesis.task.data.feature.login.datasource.LoginRemote
import com.github.genesis.task.data.feature.login.entity.LoginEntity
import com.github.genesis.task.data.remote.ApiServiceController
import io.reactivex.Observable
import javax.inject.Inject


class LoginRemoteImpl @Inject constructor(
    private val apiServiceController: ApiServiceController
) : LoginRemote {

    override fun login(loginEntity: LoginEntity): Observable<Any> {
        apiServiceController.createToken(loginEntity.login, loginEntity.password)
        return apiServiceController.getApiService().getUser()
    }
}
package com.github.genesis.task.data.cache.feature.search.result.mapper

import com.github.genesis.task.data.cache.CacheMapper
import com.github.genesis.task.data.cache.feature.search.result.model.CachedOwner
import com.github.genesis.task.data.feature.search.entity.OwnerEntity
import javax.inject.Inject

class CachedOwnerMapper @Inject constructor() : CacheMapper<CachedOwner, OwnerEntity> {

    override fun mapFromCached(source: CachedOwner): OwnerEntity =
        with(source) {
            OwnerEntity(
                receivedEventsUrl = receivedEventsUrl,
                url = ownerUrl,
                nodeId = ownerNodeId,
                id = uId,
                type = type,
                gravatarId = gravatarId,
                avatarUrl = avatarUrl,
                login = login
            )
        }

    override fun mapToCached(source: OwnerEntity): CachedOwner =
        with(source) {
            CachedOwner(
                uId = id,
                receivedEventsUrl = receivedEventsUrl,
                avatarUrl = avatarUrl,
                login = login,
                type = type,
                gravatarId = gravatarId,
                ownerUrl = url,
                ownerNodeId = nodeId
            )
        }
}
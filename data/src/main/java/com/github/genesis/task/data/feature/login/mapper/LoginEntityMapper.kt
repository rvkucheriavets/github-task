package com.github.genesis.task.data.feature.login.mapper

import com.github.genesis.task.data.EntityMapper
import com.github.genesis.task.data.feature.login.entity.LoginEntity
import com.github.genesis.task.domain.feature.login.model.Login
import javax.inject.Inject


class LoginEntityMapper @Inject constructor() : EntityMapper<LoginEntity, Login> {

    override fun mapFromEntity(source: LoginEntity): Login =
        with(source) {
            Login(
                login = login,
                password = password
            )
        }

    override fun mapToEntity(source: Login): LoginEntity =
        with(source) {
            LoginEntity(
                login = login,
                password = password
            )
        }
}
package com.github.genesis.task.data.helpers

import android.content.Context
import android.net.ConnectivityManager

class NetworkChecker(context: Context) {

    private val connectivityManager by lazy { context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager }

    fun isInternetConnectionEnabled(): Boolean {
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnectedOrConnecting
    }

    fun isConnectedOverWifi(): Boolean {
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null
            && networkInfo.isConnectedOrConnecting
            && networkInfo.type == ConnectivityManager.TYPE_WIFI
    }

    fun isConnectedOverCellular(): Boolean {
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null
            && networkInfo.isConnectedOrConnecting
            && networkInfo.type != ConnectivityManager.TYPE_WIFI
    }

    fun isInRoaming(): Boolean {
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null
            && networkInfo.isConnectedOrConnecting
            && networkInfo.type != ConnectivityManager.TYPE_WIFI
            && networkInfo.isRoaming
    }
}
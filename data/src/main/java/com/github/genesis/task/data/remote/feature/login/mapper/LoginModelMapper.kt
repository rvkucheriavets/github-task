package com.github.genesis.task.data.remote.feature.login.mapper

import com.github.genesis.task.data.feature.login.entity.LoginEntity
import com.github.genesis.task.data.remote.ModelMapper
import com.github.genesis.task.data.remote.feature.login.model.LoginModel
import javax.inject.Inject


class LoginModelMapper @Inject constructor() : ModelMapper<LoginModel, LoginEntity> {

    override fun mapFromRemote(source: LoginModel): LoginEntity =
        with(source) {
            LoginEntity(
                login = login,
                password = password
            )
        }

    override fun mapToRemote(source: LoginEntity): LoginModel =
        with(source) {
            LoginModel(
                login = login,
                password = password
            )
        }
}
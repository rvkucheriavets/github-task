package com.github.genesis.task.data.cache.feature.search.result.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.github.genesis.task.data.cache.feature.search.result.dao.CachedOwnerDao

@Entity(tableName = CachedOwnerDao.TABLE_NAME)
data class CachedOwner(
    @PrimaryKey
    val uId: Long,
    val receivedEventsUrl: String,
    val avatarUrl: String,
    val login: String,
    val type: String,
    val gravatarId: String,
    val ownerUrl: String,
    val ownerNodeId: String
)
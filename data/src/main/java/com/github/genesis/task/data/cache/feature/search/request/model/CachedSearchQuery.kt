package com.github.genesis.task.data.cache.feature.search.request.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.github.genesis.task.data.cache.feature.search.request.dao.CachedSearchQueryDao

@Entity(tableName = CachedSearchQueryDao.TABLE_NAME)
data class CachedSearchQuery(
    val query: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null
}
package com.github.genesis.task.data.feature.search.mapper

import com.github.genesis.task.data.EntityMapper
import com.github.genesis.task.data.feature.search.entity.OwnerEntity
import com.github.genesis.task.data.feature.search.entity.RepositoryEntity
import com.github.genesis.task.domain.feature.search.model.Owner
import com.github.genesis.task.domain.feature.search.model.Repository
import javax.inject.Inject

class RepositoryEntityMapper @Inject constructor(
    private val ownerEntityMapper: EntityMapper<OwnerEntity, Owner>
) : EntityMapper<RepositoryEntity, Repository> {

    override fun mapFromEntity(source: RepositoryEntity): Repository =
        with(source) {
            Repository(
                owner = ownerEntityMapper.mapFromEntity(ownerEntity),
                url = url,
                nodeId = nodeId,
                id = id,
                description = description,
                createdAt = createdAt,
                defaultBranch = defaultBranch,
                fork = fork,
                forksCount = forksCount,
                fullName = fullName,
                homepage = homepage,
                htmlUrl = htmlUrl,
                jsonMemberPrivate = jsonMemberPrivate,
                language = language,
                masterBranch = masterBranch,
                name = name,
                openIssuesCount = openIssuesCount,
                pushedAt = pushedAt,
                score = score,
                size = size,
                stargazersCount = stargazersCount,
                updatedAt = updatedAt,
                watchersCount = watchersCount,
                isViewed = isViewed
            )
        }

    override fun mapToEntity(source: Repository): RepositoryEntity =
        with(source) {
            RepositoryEntity(
                ownerEntity = ownerEntityMapper.mapToEntity(owner),
                url = url,
                nodeId = nodeId,
                id = id,
                description = description,
                createdAt = createdAt,
                defaultBranch = defaultBranch,
                fork = fork,
                forksCount = forksCount,
                fullName = fullName,
                homepage = homepage,
                htmlUrl = htmlUrl,
                jsonMemberPrivate = jsonMemberPrivate,
                language = language,
                masterBranch = masterBranch,
                name = name,
                openIssuesCount = openIssuesCount,
                pushedAt = pushedAt,
                score = score,
                size = size,
                stargazersCount = stargazersCount,
                updatedAt = updatedAt,
                watchersCount = watchersCount,
                isViewed = isViewed
            )
        }
}
package com.github.genesis.task.data

import android.arch.paging.DataSource

interface BaseDataSourceFactory<Key, Value> {

    fun addParams(params: String): DataSource.Factory<Key, Value>
}
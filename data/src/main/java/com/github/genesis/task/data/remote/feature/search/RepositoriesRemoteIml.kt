package com.github.genesis.task.data.remote.feature.search

import com.github.genesis.task.data.feature.search.datasource.RepositoriesRemote
import com.github.genesis.task.data.feature.search.entity.RepositoryEntity
import com.github.genesis.task.data.remote.ApiServiceController
import com.github.genesis.task.data.remote.ModelMapper
import com.github.genesis.task.data.remote.feature.search.model.RepositoryModel
import com.github.genesis.task.domain.feature.search.model.SearchQuery
import io.reactivex.Observable
import javax.inject.Inject

class RepositoriesRemoteIml @Inject constructor(
    private val apiServiceController: ApiServiceController,
    private val repositoryModelMapper: ModelMapper<RepositoryModel, RepositoryEntity>
) : RepositoriesRemote {

    override fun getRepositoriesByName(searchQuery: SearchQuery, page: Int, pageSize: Int): Observable<List<RepositoryEntity>> =
        with(searchQuery) {
            apiServiceController.getApiService()
                .findRepositories(query,
                    sortType?.name?.toLowerCase() ?: "",
                    sortDirection?.name?.toLowerCase() ?: "",
                    page,
                    pageSize
                ).map { repositoryModelMapper.mapFromRemote(it.items) }
        }

}
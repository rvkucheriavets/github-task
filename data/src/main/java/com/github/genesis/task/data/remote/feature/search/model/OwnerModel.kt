package com.github.genesis.task.data.remote.feature.search.model

import com.google.gson.annotations.SerializedName

data class OwnerModel(

    @field:SerializedName("received_events_url")
    val receivedEventsUrl: String?,

    @field:SerializedName("avatar_url")
    val avatarUrl: String?,

    @field:SerializedName("id")
    val id: Long?,

    @field:SerializedName("login")
    val login: String?,

    @field:SerializedName("type")
    val type: String?,

    @field:SerializedName("gravatar_id")
    val gravatarId: String?,

    @field:SerializedName("url")
    val url: String?,

    @field:SerializedName("node_id")
    val nodeId: String?
)
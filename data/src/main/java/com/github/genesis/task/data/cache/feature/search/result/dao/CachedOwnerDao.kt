package com.github.genesis.task.data.cache.feature.search.result.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction
import com.github.genesis.task.data.cache.db.BaseDao
import com.github.genesis.task.data.cache.feature.search.result.model.CachedOwner
import io.reactivex.Single

@Dao
abstract class CachedOwnerDao : BaseDao<CachedOwner> {

    @Query("SELECT * FROM $TABLE_NAME " +
        "WHERE $TABLE_NAME.uId = :ownerId " +
        "LIMIT 1")
    abstract fun getOwnerById(ownerId: Long): Single<CachedOwner>

    @Transaction
    open fun addOwners(ownerList: List<CachedOwner>) {
        ownerList.forEach {
            insert(it)
            update(it)
        }
    }

    companion object {
        const val TABLE_NAME = "towner"
    }
}
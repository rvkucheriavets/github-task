package com.github.genesis.task.data.feature.search.datasource

import android.arch.paging.ItemKeyedDataSource
import com.github.genesis.task.data.feature.search.entity.RepositoryEntity
import com.github.genesis.task.domain.feature.search.model.SearchQuery
import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class RepositoriesCacheDataStore @Inject constructor(
    private val repositoriesCache: RepositoriesCache,
    private val searchCache: SearchCache,
    private val disposable: CompositeDisposable
) : ItemKeyedDataSource<SearchQuery, RepositoryEntity>(), RepositoriesDataSource {

    private var searchQuerySubject = BehaviorRelay.create<SearchQuery>()

    override fun updateRepositoryViewedState(repositoryId: Long, isViewed: Boolean): Completable =
        repositoriesCache.updateRepositoryViewedState(repositoryId, isViewed)

    override fun getLatestSearchQueries(): Single<List<SearchQuery>> =
        searchCache.getSearchQueries()

    override fun saveSearchResultsWithQuery(resultList: List<RepositoryEntity>, searchQuery: SearchQuery): Completable =
        repositoriesCache.addNewSearchResults(searchQuery.query, resultList)

    override fun saveSearchQuery(searchQuery: SearchQuery): Completable =
        searchCache.addNewSearchQuery(searchQuery)

    override fun loadInitial(params: LoadInitialParams<SearchQuery>, callback: LoadInitialCallback<RepositoryEntity>) {
        searchQuerySubject.accept(params.requestedInitialKey)
        repositoriesCache.getSearchResults(checkNotNull(params.requestedInitialKey))
            .subscribeBy { callback.onResult(it) }
            .addTo(disposable)
    }

    override fun loadAfter(params: LoadParams<SearchQuery>, callback: LoadCallback<RepositoryEntity>) {

    }

    override fun loadBefore(params: LoadParams<SearchQuery>, callback: LoadCallback<RepositoryEntity>) {

    }

    override fun getKey(item: RepositoryEntity): SearchQuery {
        return searchQuerySubject.value
    }

    override fun clearCurrentState() {
        searchQuerySubject.accept(SearchQuery(""))
    }
}
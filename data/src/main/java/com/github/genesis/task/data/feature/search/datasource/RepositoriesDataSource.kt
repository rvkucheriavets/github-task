package com.github.genesis.task.data.feature.search.datasource

import com.github.genesis.task.data.feature.search.entity.RepositoryEntity
import com.github.genesis.task.domain.feature.search.model.SearchQuery
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface RepositoriesDataSource {

    fun updateRepositoryViewedState(repositoryId: Long, isViewed: Boolean): Completable

    fun getLatestSearchQueries(): Single<List<SearchQuery>>

    fun saveSearchQuery(searchQuery: SearchQuery): Completable

    fun saveSearchResultsWithQuery(resultList: List<RepositoryEntity>, searchQuery: SearchQuery): Completable

    fun clearCurrentState()
}
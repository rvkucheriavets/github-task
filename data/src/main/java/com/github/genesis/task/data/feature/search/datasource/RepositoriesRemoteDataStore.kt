package com.github.genesis.task.data.feature.search.datasource

import android.arch.paging.ItemKeyedDataSource
import com.github.genesis.task.data.feature.search.entity.RepositoryEntity
import com.github.genesis.task.domain.feature.search.model.SearchQuery
import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.Observables
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class RepositoriesRemoteDataStore @Inject constructor(
    private val repositoriesRemote: RepositoriesRemote,
    private val disposable: CompositeDisposable
) : ItemKeyedDataSource<SearchQuery, RepositoryEntity>(), RepositoriesDataSource {

    private var searchQueryRelay = BehaviorRelay.create<SearchQuery>()

    private var firstHalfPage = 1

    private var secondHalfPage = 1

    override fun updateRepositoryViewedState(repositoryId: Long, isViewed: Boolean): Completable {
        throw UnsupportedOperationException()
    }

    override fun getLatestSearchQueries(): Single<List<SearchQuery>> {
        throw UnsupportedOperationException()
    }

    override fun saveSearchQuery(searchQuery: SearchQuery): Completable {
        throw UnsupportedOperationException()
    }

    override fun saveSearchResultsWithQuery(resultList: List<RepositoryEntity>, searchQuery: SearchQuery): Completable {
        throw UnsupportedOperationException()
    }

    override fun loadInitial(params: LoadInitialParams<SearchQuery>, callback: LoadInitialCallback<RepositoryEntity>) {
        clearCurrentState()
        searchQueryRelay.accept(params.requestedInitialKey)
        Observable.just(params.requestedInitialKey)
            .flatMap {
                val firstHalf = repositoriesRemote.getRepositoriesByName(it, firstHalfPage, INITIAL_FIRST_HALF)

                val secondHalf = repositoriesRemote.getRepositoriesByName(it, secondHalfPage, INITIAL_SECOND_HALF)

                return@flatMap Observables.zip(firstHalf, secondHalf)
                    .map { (firstPart, secondPart) ->
                        val list = arrayListOf<RepositoryEntity>()
                        list.addAll(firstPart.subList(0, 15))
                        list.addAll(secondPart.subList(15, 30))
                        return@map list.toList()
                    }
            }
            .subscribe {
                callback.onResult(it)
            }
            .addTo(disposable)
    }

    override fun loadAfter(params: LoadParams<SearchQuery>, callback: LoadCallback<RepositoryEntity>) {
        Observable.just(params.key)
            .flatMap {
                val firstHalf = repositoriesRemote.getRepositoriesByName(it, getNextFirstHalfPageNumber(), INITIAL_FIRST_HALF)

                val secondHalf = repositoriesRemote.getRepositoriesByName(it, ++secondHalfPage, INITIAL_SECOND_HALF)

                return@flatMap Observables.zip(firstHalf, secondHalf)
                    .map { (firstPart, secondPart) ->
                        val list = arrayListOf<RepositoryEntity>()
                        if (isHalfEquals()) {
                            list.addAll(secondPart.subList(0, 15))
                            list.addAll(firstPart.subList(5, 20))
                        } else {
                            list.addAll(firstPart.subList(0, 15))
                            list.addAll(secondPart.subList(15, 30))
                        }
                        return@map list.toList()
                    }
            }
            .subscribe {
                callback.onResult(it)
            }
            .addTo(disposable)
    }

    override fun loadBefore(params: LoadParams<SearchQuery>, callback: LoadCallback<RepositoryEntity>) {
        // Keep not implemented
    }

    override fun getKey(item: RepositoryEntity): SearchQuery {
        return searchQueryRelay.value
    }

    override fun invalidate() {
        clearCurrentState()
        super.invalidate()
    }

    override fun clearCurrentState() {
        firstHalfPage = 1
        secondHalfPage = 1
        searchQueryRelay.accept(SearchQuery(""))
    }

    private fun isHalfEquals() = (firstHalfPage * INITIAL_FIRST_HALF) % (secondHalfPage * INITIAL_SECOND_HALF) == 0

    private fun getNextFirstHalfPageNumber(): Int {
        firstHalfPage = if (isHalfEquals()) {
            firstHalfPage + 1
        } else {
            firstHalfPage + 2
        }
        return firstHalfPage
    }

    companion object {
        const val INITIAL_FIRST_HALF = 20
        const val INITIAL_SECOND_HALF = 30
    }
}
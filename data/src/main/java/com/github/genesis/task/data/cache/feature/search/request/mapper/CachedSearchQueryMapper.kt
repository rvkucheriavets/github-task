package com.github.genesis.task.data.cache.feature.search.request.mapper

import com.github.genesis.task.data.cache.CacheMapper
import com.github.genesis.task.data.cache.feature.search.request.model.CachedSearchQuery
import com.github.genesis.task.domain.feature.search.model.SearchQuery
import com.github.genesis.task.domain.feature.search.model.SortDirection
import com.github.genesis.task.domain.feature.search.model.SortType
import javax.inject.Inject

class CachedSearchQueryMapper @Inject constructor() : CacheMapper<CachedSearchQuery, SearchQuery> {

    override fun mapFromCached(source: CachedSearchQuery): SearchQuery =
        with(source) {
            SearchQuery(
                query = query,
                sortType = SortType.STARS,
                sortDirection = SortDirection.DESC
            )
        }

    override fun mapToCached(source: SearchQuery): CachedSearchQuery =
        with(source) {
            CachedSearchQuery(
                query = query
            )
        }
}
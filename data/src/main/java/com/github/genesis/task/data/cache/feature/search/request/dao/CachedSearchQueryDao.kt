package com.github.genesis.task.data.cache.feature.search.request.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.github.genesis.task.data.cache.db.BaseDao
import com.github.genesis.task.data.cache.feature.search.request.model.CachedSearchQuery
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
abstract class CachedSearchQueryDao : BaseDao<CachedSearchQuery> {

    @Query("SELECT DISTINCT $TABLE_NAME.query FROM $TABLE_NAME")
    abstract fun getSearchQueries(): Single<List<CachedSearchQuery>>

    companion object {
        const val TABLE_NAME = "tquery"
    }
}
package com.github.genesis.task.data.feature.search.entity

data class RepositoryEntity(
    val ownerEntity: OwnerEntity,
    val jsonMemberPrivate: Boolean,
    val stargazersCount: Int,
    val pushedAt: String,
    val openIssuesCount: Int,
    val description: String,
    val createdAt: String,
    val language: String,
    val url: String,
    val score: Double,
    val fork: Boolean,
    val fullName: String,
    val updatedAt: String,
    val size: Int,
    val htmlUrl: String,
    val name: String,
    val defaultBranch: String,
    val id: Long,
    val watchersCount: Int,
    val masterBranch: String,
    val nodeId: String,
    val homepage: String,
    val forksCount: Int,
    val isViewed: Boolean
)
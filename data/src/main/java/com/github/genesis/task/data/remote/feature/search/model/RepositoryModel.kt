package com.github.genesis.task.data.remote.feature.search.model

import com.google.gson.annotations.SerializedName

data class RepositoryModel(

    @field:SerializedName("owner")
    val ownerModel: OwnerModel?,

    @field:SerializedName("private")
    val jsonMemberPrivate: Boolean?,

    @field:SerializedName("stargazers_count")
    val stargazersCount: Int?,

    @field:SerializedName("pushed_at")
    val pushedAt: String?,

    @field:SerializedName("open_issues_count")
    val openIssuesCount: Int?,

    @field:SerializedName("description")
    val description: String?,

    @field:SerializedName("created_at")
    val createdAt: String?,

    @field:SerializedName("language")
    val language: String?,

    @field:SerializedName("url")
    val url: String?,

    @field:SerializedName("score")
    val score: Double?,

    @field:SerializedName("fork")
    val fork: Boolean?,

    @field:SerializedName("full_name")
    val fullName: String?,

    @field:SerializedName("updated_at")
    val updatedAt: String?,

    @field:SerializedName("size")
    val size: Int?,

    @field:SerializedName("html_url")
    val htmlUrl: String?,

    @field:SerializedName("name")
    val name: String?,

    @field:SerializedName("default_branch")
    val defaultBranch: String?,

    @field:SerializedName("id")
    val id: Long?,

    @field:SerializedName("watchers_count")
    val watchersCount: Int?,

    @field:SerializedName("master_branch")
    val masterBranch: String?,

    @field:SerializedName("node_id")
    val nodeId: String?,

    @field:SerializedName("homepage")
    val homepage: String?,

    @field:SerializedName("forks_count")
    val forksCount: Int?
)
package com.github.genesis.task.data.remote.feature.search.mapper

import com.github.genesis.task.data.feature.search.entity.OwnerEntity
import com.github.genesis.task.data.feature.search.entity.RepositoryEntity
import com.github.genesis.task.data.remote.ModelMapper
import com.github.genesis.task.data.remote.feature.search.model.OwnerModel
import com.github.genesis.task.data.remote.feature.search.model.RepositoryModel
import javax.inject.Inject

class RepositoryModelMapper @Inject constructor(
    private val ownerModelMapper: ModelMapper<OwnerModel, OwnerEntity>
) : ModelMapper<RepositoryModel, RepositoryEntity> {

    override fun mapFromRemote(source: RepositoryModel): RepositoryEntity =
        with(source) {
            RepositoryEntity(
                ownerEntity = ownerModelMapper.mapFromRemote(checkNotNull(ownerModel)),
                url = checkNotNull(url),
                nodeId = checkNotNull(nodeId),
                id = checkNotNull(id),
                description = description ?: "",
                createdAt = checkNotNull(createdAt),
                defaultBranch = checkNotNull(defaultBranch),
                fork = checkNotNull(fork),
                forksCount = checkNotNull(forksCount),
                fullName = checkNotNull(fullName),
                homepage = homepage ?: "",
                htmlUrl = checkNotNull(htmlUrl),
                jsonMemberPrivate = checkNotNull(jsonMemberPrivate),
                language = language ?: "",
                masterBranch = masterBranch ?: "",
                name = checkNotNull(name),
                openIssuesCount = checkNotNull(openIssuesCount),
                pushedAt = checkNotNull(pushedAt),
                score = checkNotNull(score),
                size = checkNotNull(size),
                stargazersCount = checkNotNull(stargazersCount),
                updatedAt = checkNotNull(updatedAt),
                watchersCount = checkNotNull(watchersCount),
                isViewed = false
            )
        }

    override fun mapToRemote(source: RepositoryEntity): RepositoryModel =
        with(source) {
            RepositoryModel(
                ownerModel = ownerModelMapper.mapToRemote(ownerEntity),
                url = url,
                nodeId = nodeId,
                id = id,
                description = description,
                createdAt = createdAt,
                defaultBranch = defaultBranch,
                fork = fork,
                forksCount = forksCount,
                fullName = fullName,
                homepage = homepage,
                htmlUrl = htmlUrl,
                jsonMemberPrivate = jsonMemberPrivate,
                language = language,
                masterBranch = masterBranch,
                name = name,
                openIssuesCount = openIssuesCount,
                pushedAt = pushedAt,
                score = score,
                size = size,
                stargazersCount = stargazersCount,
                updatedAt = updatedAt,
                watchersCount = watchersCount
            )
        }
}
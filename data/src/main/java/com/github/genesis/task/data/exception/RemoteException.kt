package com.github.genesis.task.data.exception


sealed class RemoteException : RuntimeException() {

    class NoTokenException : RemoteException()
    class HttpException : RemoteException()
}
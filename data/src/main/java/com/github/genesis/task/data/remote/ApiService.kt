package com.github.genesis.task.data.remote

import com.github.genesis.task.data.remote.feature.search.model.SearchResponseModel
import com.github.genesis.task.domain.feature.search.model.SortDirection
import com.github.genesis.task.domain.feature.search.model.SortType
import io.reactivex.Flowable
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface ApiService {

    @GET("user")
    fun getUser(): Observable<Any>

    @Headers("Accept: application/vnd.github.mercy-preview+json")
    @GET("search/repositories")
    fun findRepositories(
        @Query("q") query: String,
        @Query("sort") sortType: String,
        @Query("order") sortDirection: String,
        @Query("page") page: Int,
        @Query("per_page") pageSize: Int
    ): Observable<SearchResponseModel>
}
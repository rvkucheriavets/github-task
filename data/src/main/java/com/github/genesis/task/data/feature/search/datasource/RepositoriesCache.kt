package com.github.genesis.task.data.feature.search.datasource

import com.github.genesis.task.data.feature.search.entity.RepositoryEntity
import com.github.genesis.task.domain.feature.search.model.SearchQuery
import io.reactivex.Completable
import io.reactivex.Single

interface RepositoriesCache {

    fun updateRepositoryViewedState(repositoryId: Long, isViewed: Boolean): Completable

    fun addNewSearchResults(query: String, resultList: List<RepositoryEntity>): Completable

    fun getSearchResults(searchQuery: SearchQuery): Single<List<RepositoryEntity>>
}
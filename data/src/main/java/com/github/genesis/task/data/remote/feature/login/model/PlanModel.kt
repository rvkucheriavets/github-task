package com.github.genesis.task.data.remote.feature.login.model

import com.google.gson.annotations.SerializedName

data class PlanModel(

    @field:SerializedName("private_repos")
    val privateRepos: Int? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("collaborators")
    val collaborators: Int? = null,

    @field:SerializedName("space")
    val space: Int? = null
)
package com.github.genesis.task.data.feature.login.entity

data class LoginEntity(
    val login: String,
    val password: String
)
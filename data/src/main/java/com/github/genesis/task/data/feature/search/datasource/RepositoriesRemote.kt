package com.github.genesis.task.data.feature.search.datasource

import com.github.genesis.task.data.feature.search.entity.RepositoryEntity
import com.github.genesis.task.domain.feature.search.model.SearchQuery
import io.reactivex.Observable

interface RepositoriesRemote {

    fun getRepositoriesByName(searchQuery: SearchQuery, page: Int, pageSize: Int): Observable<List<RepositoryEntity>>
}
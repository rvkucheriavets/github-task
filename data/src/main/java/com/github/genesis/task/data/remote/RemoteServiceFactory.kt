package com.github.genesis.task.data.remote

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.github.genesis.task.data.exception.RemoteException
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class RemoteServiceFactory @Inject constructor(
    isDebug: Boolean = false
) : ApiServiceController {

    private val okHttpClient: OkHttpClient

    private val tokenInterceptor: TokenInterceptor

    private val loggingInterceptor: HttpLoggingInterceptor

    private val exceptionInterceptor: ExceptionInterceptor

    init {
        tokenInterceptor = TokenInterceptor()
        loggingInterceptor = makeLoggingInterceptor(isDebug)
        exceptionInterceptor = ExceptionInterceptor()
        okHttpClient = makeOkHttpClient()

        apiService = makeApiService(okHttpClient, makeGson())
    }

    override fun getApiService(): ApiService {
        return apiService
    }

    override fun getApiToken(): String {
        return token
    }

    override fun createToken(login: String, password: String) {
        token = Credentials.basic(login, password)
    }

    private fun makeApiService(okHttpClient: OkHttpClient, gson: Gson): ApiService {
        val retrofit = Retrofit.Builder()
            .baseUrl(HOSTNAME)
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        return retrofit.create(ApiService::class.java)
    }

    private fun makeOkHttpClient(): OkHttpClient {
        val client = OkHttpClient.Builder()
            .addInterceptor(tokenInterceptor)
            .addInterceptor(loggingInterceptor)
            .addInterceptor(exceptionInterceptor)
            .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)

        return client.build()
    }

    private fun makeGson(): Gson {
        return GsonBuilder()
            .setLenient()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
            .serializeNulls()
            .create()
    }

    private fun makeLoggingInterceptor(isDebug: Boolean): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = if (isDebug)
            HttpLoggingInterceptor.Level.BODY
        else
            HttpLoggingInterceptor.Level.NONE
        return logging
    }

    inner class TokenInterceptor : Interceptor {
        override fun intercept(chain: Interceptor.Chain?): Response? {
            if (chain == null) {
                return null
            }

            val requestBuilder = chain.request()
                .newBuilder()

            if (token.isNotEmpty()) {
                requestBuilder.addHeader(AUTH_HEADER, token)
            } else {
                throw RemoteException.NoTokenException()
            }

            val request = requestBuilder.build()

            return chain.proceed(request)
        }
    }

    inner class ExceptionInterceptor : Interceptor {

        override fun intercept(chain: Interceptor.Chain?): Response? {
            if (chain == null) {
                return null
            }

            val request = chain.request()
            val response = chain.proceed(request)

            return when (response.code()) {
                in 200..210 -> response
                else -> throw RemoteException.HttpException()
            }
        }
    }

    companion object {

        private const val HOSTNAME = "https://api.github.com"

        private const val CONNECT_TIMEOUT = 30L

        private const val READ_TIMEOUT = 120L

        private const val AUTH_HEADER = "Authorization"

        private var token: String = ""

        private lateinit var apiService: ApiService
    }
}
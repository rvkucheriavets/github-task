package com.github.genesis.task.data.cache.feature.search.request

import com.github.genesis.task.data.cache.CacheMapper
import com.github.genesis.task.data.cache.db.ApplicationDatabase
import com.github.genesis.task.data.cache.feature.search.request.model.CachedSearchQuery
import com.github.genesis.task.data.feature.search.datasource.SearchCache
import com.github.genesis.task.domain.feature.search.model.SearchQuery
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class SearchQueryCacheImpl @Inject constructor(
    private val applicationDatabase: ApplicationDatabase,
    private val searchQueryMapper: CacheMapper<CachedSearchQuery, SearchQuery>
) : SearchCache {

    override fun addNewSearchQuery(query: SearchQuery): Completable =
        Completable.fromCallable {
            applicationDatabase.cachedSearchQueryDao().insert(searchQueryMapper.mapToCached(query))
        }

    override fun getSearchQueries(): Single<List<SearchQuery>> =
        applicationDatabase.cachedSearchQueryDao().getSearchQueries()
            .map { searchQueryMapper.mapFromCached(it) }
}
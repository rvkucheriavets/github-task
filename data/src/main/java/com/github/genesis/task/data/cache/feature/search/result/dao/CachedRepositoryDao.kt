package com.github.genesis.task.data.cache.feature.search.result.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction
import com.github.genesis.task.data.cache.db.BaseDao
import com.github.genesis.task.data.cache.feature.search.result.model.CachedRepository
import io.reactivex.Single

@Dao
abstract class CachedRepositoryDao : BaseDao<CachedRepository> {

    @Query("SELECT * FROM $TABLE_NAME " +
        "JOIN ${CachedOwnerDao.TABLE_NAME} " +
        "ON ${CachedOwnerDao.TABLE_NAME}.uId == $TABLE_NAME.ownerId " +
        "WHERE $TABLE_NAME.searchQuery = :query " +
        "ORDER BY $TABLE_NAME.stargazersCount DESC")
    abstract fun getRepositoriesBySearchQuery(query: String): Single<List<CachedRepository>>

    @Query("UPDATE $TABLE_NAME SET " +
        "isViewed = :isViewed " +
        "WHERE id = :repositoryId")
    abstract fun updateRepositoryViewedState(repositoryId: Long, isViewed: Boolean)

    @Transaction
    open fun addSearchResults(resultList: List<CachedRepository>) {
        resultList.forEach {
            insert(it)
            update(it)
        }
    }

    companion object {
        const val TABLE_NAME = "trepositories"
    }
}
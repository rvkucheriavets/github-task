package com.github.genesis.task.data.feature.search.mapper

import com.github.genesis.task.data.EntityMapper
import com.github.genesis.task.data.feature.search.entity.OwnerEntity
import com.github.genesis.task.domain.feature.search.model.Owner
import javax.inject.Inject

class OwnerEntityMapper @Inject constructor() : EntityMapper<OwnerEntity, Owner> {

    override fun mapFromEntity(source: OwnerEntity): Owner =
        with(source) {
            Owner(
                receivedEventsUrl = receivedEventsUrl,
                login = login,
                id = id,
                avatarUrl = avatarUrl,
                gravatarId = gravatarId,
                nodeId = nodeId,
                type = type,
                url = url
            )
        }

    override fun mapToEntity(source: Owner): OwnerEntity =
        with(source) {
            OwnerEntity(
                receivedEventsUrl = receivedEventsUrl,
                login = login,
                id = id,
                avatarUrl = avatarUrl,
                gravatarId = gravatarId,
                nodeId = nodeId,
                type = type,
                url = url
            )
        }
}
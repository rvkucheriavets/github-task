package com.github.genesis.task.data.cache

interface CacheMapper<C, E> {

    fun mapFromCached(source: C): E

    fun mapToCached(source: E): C

    fun mapFromCached(source: List<C>): List<E> = source.map { mapFromCached(it) }

    fun mapToCached(source: List<E>): List<C> = source.map { mapToCached(it) }
}
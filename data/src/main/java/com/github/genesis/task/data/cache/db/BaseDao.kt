package com.github.genesis.task.data.cache.db

import android.arch.persistence.room.*

/**
 * @author r.kucheriavets
 */
@Dao
interface BaseDao<in T> {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(obj: T)

    @Update(onConflict = OnConflictStrategy.IGNORE)
    fun update(data: T)

    @Delete
    fun delete(obj: T)
}
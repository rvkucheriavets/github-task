package com.github.genesis.task.data.cache.feature.search.result.model

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.github.genesis.task.data.cache.feature.search.result.dao.CachedRepositoryDao

@Entity(tableName = CachedRepositoryDao.TABLE_NAME)
data class CachedRepository @Ignore constructor(
    @PrimaryKey
    val id: Long,
    val jsonMemberPrivate: Boolean,
    val stargazersCount: Int,
    val pushedAt: String,
    val openIssuesCount: Int,
    val description: String,
    val createdAt: String,
    val language: String,
    val url: String,
    val score: Double,
    val fork: Boolean,
    val fullName: String,
    val updatedAt: String,
    val size: Int,
    val htmlUrl: String,
    val name: String,
    val defaultBranch: String,
    val watchersCount: Int,
    val masterBranch: String,
    val nodeId: String,
    val homepage: String,
    val forksCount: Int,
    val ownerId: Long,
    val isViewed: Boolean = false
) {

    var searchQuery: String? = null

    @Embedded
    var owner: CachedOwner? = null

    constructor(
        id: Long,
        jsonMemberPrivate: Boolean,
        stargazersCount: Int,
        pushedAt: String,
        openIssuesCount: Int,
        description: String,
        createdAt: String,
        language: String,
        url: String,
        score: Double,
        fork: Boolean,
        fullName: String,
        updatedAt: String,
        size: Int,
        htmlUrl: String,
        name: String,
        defaultBranch: String,
        watchersCount: Int,
        masterBranch: String,
        nodeId: String,
        homepage: String,
        forksCount: Int,
        ownerId: Long,
        searchQuery: String,
        owner: CachedOwner,
        isViewed: Boolean
    ) : this(
        id,
        jsonMemberPrivate,
        stargazersCount,
        pushedAt,
        openIssuesCount,
        description,
        createdAt,
        language,
        url,
        score,
        fork,
        fullName,
        updatedAt,
        size,
        htmlUrl,
        name,
        defaultBranch,
        watchersCount,
        masterBranch,
        nodeId,
        homepage,
        forksCount,
        ownerId,
        isViewed
    ) {
        this.owner = owner
        this.searchQuery = searchQuery
    }
}
package com.github.genesis.task.data.feature.search.datasource

import android.arch.paging.DataSource
import com.github.genesis.task.data.feature.search.entity.RepositoryEntity
import com.github.genesis.task.data.helpers.NetworkChecker
import com.github.genesis.task.data.remote.ApiServiceController
import com.github.genesis.task.domain.feature.search.model.SearchQuery
import javax.inject.Inject

class RepositoriesDataSourceFactory @Inject constructor(
    private val networkChecker: NetworkChecker,
    private val apiServiceController: ApiServiceController,
    private val remoteDataStore: RepositoriesRemoteDataStore,
    private val cacheDataStore: RepositoriesCacheDataStore
) : DataSource.Factory<SearchQuery, RepositoryEntity>() {

    override fun create(): DataSource<SearchQuery, RepositoryEntity> {
        return if (isCurrentDataSourceCache())
            cacheDataStore
        else
            remoteDataStore
    }

    fun getRemoteDataStore(): RepositoriesDataSource = remoteDataStore

    fun getCacheDataStore(): RepositoriesDataSource = cacheDataStore

    fun isCurrentDataSourceCache(): Boolean =
        apiServiceController.getApiToken().isBlank() ||
            !networkChecker.isInternetConnectionEnabled()
}
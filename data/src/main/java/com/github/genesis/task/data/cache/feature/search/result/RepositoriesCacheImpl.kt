package com.github.genesis.task.data.cache.feature.search.result

import com.github.genesis.task.data.cache.CacheMapper
import com.github.genesis.task.data.cache.db.ApplicationDatabase
import com.github.genesis.task.data.cache.feature.search.result.model.CachedOwner
import com.github.genesis.task.data.cache.feature.search.result.model.CachedRepository
import com.github.genesis.task.data.feature.search.datasource.RepositoriesCache
import com.github.genesis.task.data.feature.search.entity.OwnerEntity
import com.github.genesis.task.data.feature.search.entity.RepositoryEntity
import com.github.genesis.task.domain.feature.search.model.SearchQuery
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class RepositoriesCacheImpl @Inject constructor(
    private val applicationDatabase: ApplicationDatabase,
    private val repositoryMapper: CacheMapper<CachedRepository, RepositoryEntity>,
    private val ownerMapper: CacheMapper<CachedOwner, OwnerEntity>
) : RepositoriesCache {

    override fun addNewSearchResults(query: String, resultList: List<RepositoryEntity>): Completable =
        Completable.fromCallable {
            with(applicationDatabase) {
                cachedOwnerDao().addOwners(resultList.map { ownerMapper.mapToCached(it.ownerEntity) })
                cachedRepositoryDao().addSearchResults(
                    resultList.map { repositoryMapper.mapToCached(it).apply { searchQuery = query } })
            }
        }

    override fun getSearchResults(searchQuery: SearchQuery): Single<List<RepositoryEntity>> =
        applicationDatabase.cachedRepositoryDao().getRepositoriesBySearchQuery(searchQuery.query)
            .map { repositoryMapper.mapFromCached(it) }

    override fun updateRepositoryViewedState(repositoryId: Long, isViewed: Boolean): Completable =
        Completable.fromCallable {
            applicationDatabase.cachedRepositoryDao().updateRepositoryViewedState(repositoryId, isViewed)
        }
}
package com.github.genesis.task.data.remote

interface ApiServiceController {

    fun getApiService(): ApiService

    fun getApiToken(): String

    fun createToken(login: String, password: String)
}

package com.github.genesis.task.data.cache.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.github.genesis.task.data.cache.feature.search.request.dao.CachedSearchQueryDao
import com.github.genesis.task.data.cache.feature.search.request.model.CachedSearchQuery
import com.github.genesis.task.data.cache.feature.search.result.dao.CachedOwnerDao
import com.github.genesis.task.data.cache.feature.search.result.dao.CachedRepositoryDao
import com.github.genesis.task.data.cache.feature.search.result.model.CachedOwner
import com.github.genesis.task.data.cache.feature.search.result.model.CachedRepository
import javax.inject.Inject

@Database(entities = arrayOf(
    CachedRepository::class,
    CachedOwner::class,
    CachedSearchQuery::class
), version = 1)
@TypeConverters(Converters::class)
abstract class ApplicationDatabase @Inject constructor() : RoomDatabase() {

    abstract fun cachedRepositoryDao(): CachedRepositoryDao

    abstract fun cachedOwnerDao(): CachedOwnerDao

    abstract fun cachedSearchQueryDao(): CachedSearchQueryDao
}
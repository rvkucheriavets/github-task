package com.github.genesis.task.data.feature.login.repository

import com.github.genesis.task.data.EntityMapper
import com.github.genesis.task.data.feature.login.datasource.UserCacheDataStore
import com.github.genesis.task.data.feature.login.datasource.UserRemoteDataStore
import com.github.genesis.task.data.feature.login.entity.LoginEntity
import com.github.genesis.task.domain.feature.login.model.Login
import com.github.genesis.task.domain.feature.login.repository.UserRepository
import io.reactivex.Observable
import javax.inject.Inject

class UserDataRepository @Inject constructor(
    private val cacheDataSource: UserCacheDataStore,
    private val remoteDataStore: UserRemoteDataStore,
    private val loginEntityMapper: EntityMapper<LoginEntity, Login>
) : UserRepository {

    override fun login(login: Login): Observable<Any> =
        remoteDataStore.login(loginEntityMapper.mapToEntity(login))
}
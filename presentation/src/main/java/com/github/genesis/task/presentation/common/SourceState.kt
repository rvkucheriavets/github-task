package com.github.genesis.task.presentation.common

enum class SourceState {
    LOADING,
    SUCCESS,
    ERROR
}
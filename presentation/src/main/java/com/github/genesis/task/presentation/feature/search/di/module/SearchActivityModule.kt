package com.github.genesis.task.presentation.feature.search.di.module

import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedListAdapter
import com.github.genesis.task.domain.feature.search.model.Repository
import com.github.genesis.task.presentation.feature.search.adapter.RepositoriesAdapter
import com.github.genesis.task.presentation.feature.search.viewmodel.SearchViewModel
import com.github.genesis.task.presentation.injection.scopes.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class SearchActivityModule {

    @Binds
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    abstract fun bindSearchViewModel(viewModel: SearchViewModel): ViewModel

    @Binds
    abstract fun bindRepositoriesAdapter(adapter: RepositoriesAdapter): PagedListAdapter<Repository, RepositoriesAdapter.ViewHolder>
}
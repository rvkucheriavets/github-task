package com.github.genesis.task.presentation.injection.module

import android.arch.lifecycle.ViewModelProvider
import com.github.genesis.task.presentation.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory
}
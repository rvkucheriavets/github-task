package com.github.genesis.task.presentation

interface DataMapper<out D, in S> {

    fun mapToObject(source: S): D

    fun mapToObjects(source: List<S>): List<D> = source.map { mapToObject(it) }
}
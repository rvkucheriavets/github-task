package com.github.genesis.task.presentation.extension

import android.view.View
import android.view.ViewTreeObserver

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.hide() {
    this.visibility = View.GONE
}

fun View.disappear() {
    this.visibility = View.INVISIBLE
}

fun View.toggleView(show: Boolean) {
    if (show) this.show()
    else this.hide()
}

fun <T : View> T.scaleBoth(scale: Float) {
    scaleY = scale
    scaleX = scale
}

fun <T : View> T.toggleVisibilityState(state: Boolean,
                                       defaultTrueState: Int = View.VISIBLE,
                                       defaultFalseState: Int = View.GONE) {
    visibility = if (state) defaultTrueState else defaultFalseState
}

fun View.toggleSelected() {
    isSelected = !isSelected
}

inline fun <T : View> T.afterMeasured(crossinline f: T.() -> Unit) {
    viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            if (measuredWidth > 0 && measuredHeight > 0) {
                viewTreeObserver.removeOnGlobalLayoutListener(this)
                f()
            }
        }
    })
}
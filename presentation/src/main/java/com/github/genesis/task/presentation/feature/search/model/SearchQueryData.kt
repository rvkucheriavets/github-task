package com.github.genesis.task.presentation.feature.search.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SearchQueryData(
    val query: String,
    val sortTypeData: SortTypeData? = null,
    val sortDirectionData: SortDirectionData? = null
) : Parcelable
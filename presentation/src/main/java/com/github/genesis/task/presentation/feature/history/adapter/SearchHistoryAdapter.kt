package com.github.genesis.task.presentation.feature.history.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.genesis.task.presentation.R
import com.github.genesis.task.presentation.common.Click
import com.github.genesis.task.presentation.feature.search.model.SearchQueryData
import com.github.genesis.task.presentation.util.RxBus
import kotlinx.android.synthetic.main.item_search_history.view.*
import javax.inject.Inject

class SearchHistoryAdapter @Inject constructor(
    private val bus: RxBus
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val items = arrayListOf<SearchQueryData>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_search_history, parent, false)
        return ViewHolder(view).apply {
            view.setOnClickListener {
                bus.clicksConsumer.accept(Click.SearchHistoryItemClick(items[adapterPosition]))
            }
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            holder.setData(items[position])
        }
    }

    fun setItems(list: List<SearchQueryData>) {
        items.addAll(list)
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun setData(item: SearchQueryData) {
            with(view) {
                searchQueryView.text = item.query
            }
        }
    }
}
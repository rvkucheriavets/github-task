package com.github.genesis.task.presentation.feature.search.mapper

import com.github.genesis.task.domain.feature.search.model.SearchQuery
import com.github.genesis.task.domain.feature.search.model.SortDirection
import com.github.genesis.task.domain.feature.search.model.SortType
import com.github.genesis.task.presentation.DataMapper
import com.github.genesis.task.presentation.feature.search.model.SearchQueryData
import com.github.genesis.task.presentation.feature.search.model.SortDirectionData
import com.github.genesis.task.presentation.feature.search.model.SortTypeData
import javax.inject.Inject

class SearchQueryMapper @Inject constructor() : DataMapper<SearchQuery, SearchQueryData> {

    override fun mapToObject(source: SearchQueryData): SearchQuery =
        with(source) {
            SearchQuery(
                query,
                sortTypeMapper(sortTypeData),
                sortDirectionMapper(sortDirectionData)
            )
        }

    private fun sortTypeMapper(sortTypeData: SortTypeData?) =
        when (sortTypeData) {
            SortTypeData.STARS -> SortType.STARS
            else -> null
        }

    private fun sortDirectionMapper(sortDirectionData: SortDirectionData?) =
        when (sortDirectionData) {
            SortDirectionData.ASC -> SortDirection.ASC
            SortDirectionData.DESC -> SortDirection.DESC
            else -> null
        }
}
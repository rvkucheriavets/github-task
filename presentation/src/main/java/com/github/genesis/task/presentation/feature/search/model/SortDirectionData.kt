package com.github.genesis.task.presentation.feature.search.model

enum class SortDirectionData {
    ASC, DESC
}
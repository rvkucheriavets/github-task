package com.github.genesis.task.presentation.feature.search.mapper

import com.github.genesis.task.domain.feature.search.model.SearchQuery
import com.github.genesis.task.domain.feature.search.model.SortDirection
import com.github.genesis.task.domain.feature.search.model.SortType
import com.github.genesis.task.presentation.DataMapper
import com.github.genesis.task.presentation.feature.search.model.SearchQueryData
import com.github.genesis.task.presentation.feature.search.model.SortDirectionData
import com.github.genesis.task.presentation.feature.search.model.SortTypeData
import javax.inject.Inject

class SearchQueryDataMapper @Inject constructor() : DataMapper<SearchQueryData, SearchQuery> {

    override fun mapToObject(source: SearchQuery): SearchQueryData =
        with(source) {
            SearchQueryData(
                query,
                sortTypeData = sortTypeMapper(sortType),
                sortDirectionData = sortDirectionMapper(sortDirection)
            )
        }

    private fun sortTypeMapper(sortType: SortType?) =
        when (sortType) {
            SortType.STARS -> SortTypeData.STARS
            else -> null
        }

    private fun sortDirectionMapper(sortDirection: SortDirection?) =
        when (sortDirection) {
            SortDirection.ASC -> SortDirectionData.ASC
            SortDirection.DESC -> SortDirectionData.DESC
            else -> null
        }
}
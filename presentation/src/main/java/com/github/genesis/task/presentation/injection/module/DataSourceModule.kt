package com.github.genesis.task.presentation.injection.module

import com.github.genesis.task.data.cache.db.ApplicationDatabase
import com.github.genesis.task.data.cache.feature.search.request.SearchQueryCacheImpl
import com.github.genesis.task.data.cache.feature.search.request.mapper.CachedSearchQueryMapper
import com.github.genesis.task.data.cache.feature.search.result.RepositoriesCacheImpl
import com.github.genesis.task.data.cache.feature.search.result.mapper.CachedOwnerMapper
import com.github.genesis.task.data.cache.feature.search.result.mapper.CachedRepositoryMapper
import com.github.genesis.task.data.feature.login.datasource.LoginRemote
import com.github.genesis.task.data.feature.search.datasource.RepositoriesCache
import com.github.genesis.task.data.feature.search.datasource.RepositoriesRemote
import com.github.genesis.task.data.feature.search.datasource.SearchCache
import com.github.genesis.task.data.remote.ApiServiceController
import com.github.genesis.task.data.remote.feature.login.LoginRemoteImpl
import com.github.genesis.task.data.remote.feature.search.RepositoriesRemoteIml
import com.github.genesis.task.data.remote.feature.search.mapper.OwnerModelMapper
import com.github.genesis.task.data.remote.feature.search.mapper.RepositoryModelMapper
import com.github.genesis.task.presentation.injection.scopes.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
class DataSourceModule {

    @Provides
    @ApplicationScope
    internal fun provideLoginRemote(
        apiServiceController: ApiServiceController
    ): LoginRemote {
        return LoginRemoteImpl(apiServiceController)
    }

    @Provides
    @ApplicationScope
    internal fun provideRepositoriesRemote(
        apiServiceController: ApiServiceController,
        repositoryModelMapper: RepositoryModelMapper
    ): RepositoriesRemote {
        return RepositoriesRemoteIml(apiServiceController, repositoryModelMapper)
    }

    @Provides
    @ApplicationScope
    internal fun provideRepositoriesCache(
        applicationDatabase: ApplicationDatabase,
        repositoryMapper: CachedRepositoryMapper,
        ownerMapper: CachedOwnerMapper
    ): RepositoriesCache {
        return RepositoriesCacheImpl(
            applicationDatabase,
            repositoryMapper,
            ownerMapper
        )
    }

    @Provides
    @ApplicationScope
    internal fun provideSearchCache(
        applicationDatabase: ApplicationDatabase,
        searchQueryMapper: CachedSearchQueryMapper
    ): SearchCache {
        return SearchQueryCacheImpl(
            applicationDatabase,
            searchQueryMapper
        )
    }
}
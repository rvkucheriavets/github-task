package com.github.genesis.task.presentation.feature.history.viewmodel

import android.arch.lifecycle.MutableLiveData
import com.github.genesis.task.domain.feature.search.model.SearchQuery
import com.github.genesis.task.domain.feature.search.usecase.LatestSearchQueriesUseCase
import com.github.genesis.task.presentation.BaseViewModel
import com.github.genesis.task.presentation.DataMapper
import com.github.genesis.task.presentation.common.Click
import com.github.genesis.task.presentation.common.SingleLiveEvent
import com.github.genesis.task.presentation.feature.search.mapper.SearchQueryDataMapper
import com.github.genesis.task.presentation.feature.search.model.SearchQueryData
import com.github.genesis.task.presentation.util.RxBus
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class SearchHistoryViewModel @Inject constructor(
    private val searchQueryDataMapper: SearchQueryDataMapper,
    latestSearchQueriesUseCase: LatestSearchQueriesUseCase,
    bus: RxBus
) : BaseViewModel() {

    val latestSearchQueryLiveData = MutableLiveData<List<SearchQueryData>>()

    val openSearchResults = SingleLiveEvent<SearchQueryData>()

    init {
        latestSearchQueriesUseCase.execute()
            .map { it.map { searchQueryDataMapper.mapToObject(it) } }
            .subscribeBy { latestSearchQueryLiveData.postValue(it) }
            .addTo(disposable)

        bus.clicksObservable
            .filter { it is Click }
            .map { it as Click }
            .doOnNext { handleClick(it) }
            .subscribe()
            .addTo(disposable)
    }

    private fun handleClick(click: Click) {
        when (click) {
            is Click.SearchHistoryItemClick -> {
                openSearchResults.value = click.item
            }
        }
    }
}
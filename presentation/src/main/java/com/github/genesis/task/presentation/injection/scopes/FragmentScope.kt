package com.github.genesis.task.presentation.injection.scopes

import javax.inject.Scope

/**
 * @author r_kucheriavets
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class FragmentScope
package com.github.genesis.task.presentation.feature.login.mapper

import com.github.genesis.task.domain.feature.login.model.Login
import com.github.genesis.task.presentation.DataMapper
import com.github.genesis.task.presentation.feature.login.model.LoginData
import javax.inject.Inject

open class LoginDataMapper @Inject constructor() : DataMapper<Login, LoginData> {

    override fun mapToObject(source: LoginData): Login =
        with(source) {
            Login(login, password)
        }
}
package com.github.genesis.task.presentation.injection.module

import com.github.genesis.task.presentation.feature.history.SearchHistoryActivity
import com.github.genesis.task.presentation.feature.history.di.module.SearchHistoryActivityModule
import com.github.genesis.task.presentation.feature.login.LoginActivity
import com.github.genesis.task.presentation.feature.login.di.module.LoginActivityModule
import com.github.genesis.task.presentation.feature.search.SearchActivity
import com.github.genesis.task.presentation.feature.search.di.module.SearchActivityModule
import com.github.genesis.task.presentation.injection.scopes.ActivityScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [LoginActivityModule::class])
    abstract fun bindLoginActivity(): LoginActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [SearchActivityModule::class])
    abstract fun bindSearchActivity(): SearchActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [SearchHistoryActivityModule::class])
    abstract fun bindSearchHistoryActivity(): SearchHistoryActivity
}
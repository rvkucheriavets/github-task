package com.github.genesis.task.presentation

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import dagger.android.support.AndroidSupportInjection
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

abstract class BaseFragment<F : ViewModelProvider.Factory, V : BaseViewModel> : Fragment() {

    @Inject
    lateinit var viewModelFactory: F

    lateinit var viewModel: V

    lateinit var errorDialogListener: ErrorDialogListener

    protected val disposable = CompositeDisposable()

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        try {
            errorDialogListener = context as ErrorDialogListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString() + " must implement ErrorDialogListener")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
        initViewModel()

        baseActionLiveData()
        initActionLiveData()
        initLiveData()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }

    abstract fun initListeners()

    abstract fun initViewModel()

    abstract fun initActionLiveData()

    abstract fun initLiveData()

    private fun baseActionLiveData() {
        viewModel.errorLiveEvent.observe(this, Observer {
            errorDialogListener.showErrorDialog(
                title = getString(R.string.dialog_error_title_default),
                message = getString(R.string.dialog_error_message_default)
            )
        })
    }

    protected fun setViewModel(clazz: Class<V>) {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(clazz)
    }

    protected fun Disposable.addToCompositeDisposable() {
        disposable.add(this)
    }
}
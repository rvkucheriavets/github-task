package com.github.genesis.task.presentation.widget.empty

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import com.github.genesis.task.presentation.R
import kotlinx.android.synthetic.main.view_empty.view.*

/**
 * Widget used to display an empty state to the user
 */
class EmptyView@JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {

    var emptyListener: EmptyListener? = null

    init {
        LayoutInflater.from(context).inflate(R.layout.view_empty, this)
        button_check_again.setOnClickListener { emptyListener?.onCheckAgainClicked() }
    }

}
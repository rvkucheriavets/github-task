package com.github.genesis.task.presentation.injection.module

import android.app.Application
import android.arch.lifecycle.ViewModelProvider
import android.arch.persistence.room.Room
import android.content.Context
import com.github.genesis.task.data.cache.db.ApplicationDatabase
import com.github.genesis.task.data.executor.JobExecutor
import com.github.genesis.task.data.helpers.NetworkChecker
import com.github.genesis.task.data.helpers.PreferencesHelper
import com.github.genesis.task.data.remote.ApiService
import com.github.genesis.task.data.remote.ApiServiceController
import com.github.genesis.task.data.remote.RemoteServiceFactory
import com.github.genesis.task.domain.executor.PostExecutionThread
import com.github.genesis.task.domain.executor.ThreadExecutor
import com.github.genesis.task.presentation.BuildConfig
import com.github.genesis.task.presentation.UiThread
import com.github.genesis.task.presentation.injection.scopes.ApplicationScope
import com.github.genesis.task.presentation.util.RxBus
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@Module
open class ApplicationModule {

    @Provides
    @ApplicationScope
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @ApplicationScope
    internal fun provideRxBus(): RxBus {
        return RxBus()
    }

    @Provides
    @ApplicationScope
    internal fun providePreferencesHelper(context: Context): PreferencesHelper {
        return PreferencesHelper(context)
    }

    @Provides
    @ApplicationScope
    internal fun provideNetworkChecker(context: Context): NetworkChecker {
        return NetworkChecker(context)
    }

    @Provides
    @ApplicationScope
    internal fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    @ApplicationScope
    internal fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor {
        return jobExecutor
    }

    @Provides
    @ApplicationScope
    internal fun providePostExecutionThread(uiThread: UiThread): PostExecutionThread {
        return uiThread
    }

    @Provides
    @ApplicationScope
    internal fun provideDatabase(application: Application): ApplicationDatabase {
        return Room.databaseBuilder(application.applicationContext,
            ApplicationDatabase::class.java, "github.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @ApplicationScope
    internal fun provideRemoteServiceFactory(): ApiServiceController {
        return RemoteServiceFactory(BuildConfig.DEBUG)
    }

    @Provides
    @ApplicationScope
    internal fun provideApiService(
        apiServiceController: ApiServiceController
    ): ApiService {
        return apiServiceController.getApiService()
    }
}

package com.github.genesis.task.presentation.common

open class  SourceStateHolder<out T> constructor(
    val status: SourceState,
    val data: T? = null,
    val message: String? = null
) {

    companion object {
        fun <T> success(data: T): SourceStateHolder<T> {
            return SourceStateHolder(SourceState.SUCCESS, data, null)
        }

        fun <T> error(message: String?, data: T? = null): SourceStateHolder<T> {
            return SourceStateHolder(SourceState.ERROR, null, message)
        }

        fun <T> loading(): SourceStateHolder<T> {
            return SourceStateHolder(SourceState.LOADING, null, null)
        }
    }
}
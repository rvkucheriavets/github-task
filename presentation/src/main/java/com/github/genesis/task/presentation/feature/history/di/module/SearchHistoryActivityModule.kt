package com.github.genesis.task.presentation.feature.history.di.module

import android.arch.lifecycle.ViewModel
import android.support.v7.widget.RecyclerView
import com.github.genesis.task.presentation.feature.history.adapter.SearchHistoryAdapter
import com.github.genesis.task.presentation.feature.history.viewmodel.SearchHistoryViewModel
import com.github.genesis.task.presentation.injection.scopes.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class SearchHistoryActivityModule {

    @Binds
    @IntoMap
    @ViewModelKey(SearchHistoryViewModel::class)
    abstract fun bindSearchHistoryViewModel(viewModel: SearchHistoryViewModel): ViewModel

    @Binds
    abstract fun bindSearchHistoryAdapter(adapter: SearchHistoryAdapter): RecyclerView.Adapter<RecyclerView.ViewHolder>
}
package com.github.genesis.task.presentation.feature.login.viewmodel

import android.arch.lifecycle.MutableLiveData
import com.github.genesis.task.domain.feature.login.usecase.LoginUseCase
import com.github.genesis.task.presentation.BaseViewModel
import com.github.genesis.task.presentation.common.SourceStateHolder
import com.github.genesis.task.presentation.feature.login.mapper.LoginDataMapper
import com.github.genesis.task.presentation.feature.login.model.LoginData
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val loginUseCase: LoginUseCase,
    private val loginDataMapper: LoginDataMapper
) : BaseViewModel() {

    private val loginParamsLiveData = MutableLiveData<LoginData>()

    val loginStateLiveData = MutableLiveData<SourceStateHolder<Any>>()

    init {
        loginParamsLiveData.value = LoginData()
    }

    fun setLogin(login: String) {
        if (login.isNotBlank()) {
            loginParamsLiveData.value = loginParamsLiveData.value?.copy(login = login)
        }
    }

    fun setPassword(password: String) {
        if (password.isNotBlank()) {
            loginParamsLiveData.value = loginParamsLiveData.value?.copy(password = password)
        }
    }

    fun onLoginButtonClicked() {
        loginParamsLiveData.value?.let {
            loginUseCase.execute(params = LoginUseCase.Param(loginDataMapper.mapToObject(it)))
                .doOnSubscribe { loginStateLiveData.postValue(SourceStateHolder.loading()) }
                .subscribe(
                    { loginStateLiveData.postValue(SourceStateHolder.success(it)) },
                    { loginStateLiveData.postValue(SourceStateHolder.error(it.message)) }
                ).addTo(disposable)
        }
    }

    companion object {
        val TAG = LoginViewModel::class.java.simpleName
    }
}
package com.github.genesis.task.presentation.feature.login

import android.arch.lifecycle.Observer
import android.os.Bundle
import com.github.genesis.task.presentation.BaseActivity
import com.github.genesis.task.presentation.R
import com.github.genesis.task.presentation.common.SourceState
import com.github.genesis.task.presentation.extension.hide
import com.github.genesis.task.presentation.extension.hideKeyboard
import com.github.genesis.task.presentation.extension.show
import com.github.genesis.task.presentation.feature.history.SearchHistoryActivity
import com.github.genesis.task.presentation.feature.login.viewmodel.LoginViewModel
import com.github.genesis.task.presentation.feature.search.SearchActivity
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxTextView
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.view_loading.*
import java.util.concurrent.TimeUnit

class LoginActivity : BaseActivity<LoginViewModel>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initListeners()
        bindEditTexts()
    }

    override fun initViewModel() {
        setViewModel(LoginViewModel::class.java)
    }

    override fun initActionLiveEvent() {

    }

    override fun initLiveData() {
        viewModel.loginStateLiveData.observe(this, Observer {
            it?.let { handleDataState(it.status, it.data, it.message) }
        })
    }

    override fun initListeners() {
        RxView.clicks(anonymousLoginView)
            .debounce(MIN_CLICK_INTERVAL, TimeUnit.MILLISECONDS)
            .subscribe {
                startActivity(SearchHistoryActivity.getIntent(this))
            }.addToCompositeDisposable()

        RxView.clicks(loginButtonView)
            .debounce(MIN_CLICK_INTERVAL, TimeUnit.MILLISECONDS)
            .subscribe {
                hideKeyboard()
                viewModel.onLoginButtonClicked()
            }.addToCompositeDisposable()
    }

    private fun bindEditTexts() {
        RxTextView.textChanges(loginEditView)
            .subscribe {
                viewModel.setLogin(it.toString())
            }
            .addToCompositeDisposable()
        RxTextView.textChanges(passwordEditView)
            .subscribe {
                viewModel.setPassword(it.toString())
            }
            .addToCompositeDisposable()
    }

    private fun handleDataState(sourceState: SourceState, data: Any?,
                                message: String?) {
        when (sourceState) {
            SourceState.LOADING -> setupScreenForLoadingState()
            SourceState.SUCCESS -> setupScreenForSuccess(data)
            SourceState.ERROR -> setupScreenForError(message, data)
        }
    }

    private fun setupScreenForLoadingState() {
        loginButtonView.isEnabled = false
        loginEditView.isEnabled = false
        passwordEditView.isEnabled = false
        loadingView.show()
    }

    private fun setupScreenForSuccess(data: Any?) {
        startActivity(SearchActivity.getIntent(this))
        finishAffinity()
    }

    private fun setupScreenForError(message: String?, data: Any?) {
        showErrorDialog(
            getString(R.string.dialog_error_title_default),
            getString(R.string.login_dialog_error_content)
        ) {
            loginButtonView.isEnabled = true
            loginEditView.isEnabled = true
            passwordEditView.isEnabled = true
            loadingView.hide()
        }
    }

    companion object {
        val TAG = LoginActivity::class.java.simpleName
    }
}
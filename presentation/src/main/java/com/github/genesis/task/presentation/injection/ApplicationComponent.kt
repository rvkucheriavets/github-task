package com.github.genesis.task.presentation.injection

import android.app.Application
import com.github.genesis.task.presentation.GitHubApplication
import com.github.genesis.task.presentation.injection.module.ActivityBindingModule
import com.github.genesis.task.presentation.injection.module.ApplicationModule
import com.github.genesis.task.presentation.injection.module.DataSourceModule
import com.github.genesis.task.presentation.injection.module.MapperModule
import com.github.genesis.task.presentation.injection.module.RepositoryModule
import com.github.genesis.task.presentation.injection.module.ViewModelFactoryModule
import com.github.genesis.task.presentation.injection.scopes.ApplicationScope
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule

@ApplicationScope
@Component(modules = [
    ApplicationModule::class,
    ActivityBindingModule::class,
    RepositoryModule::class,
    DataSourceModule::class,
    MapperModule::class,
    ViewModelFactoryModule::class,
    AndroidSupportInjectionModule::class
])
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

    fun inject(app: GitHubApplication)

}

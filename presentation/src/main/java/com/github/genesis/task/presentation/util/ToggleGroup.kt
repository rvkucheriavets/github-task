package com.github.genesis.task.presentation.util

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.constraint.Group
import android.util.AttributeSet
import android.view.View

class ToggleGroup @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : Group(context, attrs, defStyleAttr) {

    override fun updatePreLayout(container: ConstraintLayout) {
    }

    fun toggle() {
        val container = this.parent as ConstraintLayout
        for (i in 0 until this.mCount) {
            val id = this.mIds[i]
            val view = container.getViewById(id)
            if (view != null) {
                view.visibility = if (view.visibility == View.VISIBLE) View.INVISIBLE else View.VISIBLE
            }
        }
    }
}
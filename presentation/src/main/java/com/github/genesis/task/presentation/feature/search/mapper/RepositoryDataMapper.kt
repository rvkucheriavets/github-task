package com.github.genesis.task.presentation.feature.search.mapper

import com.github.genesis.task.domain.feature.search.model.Repository
import com.github.genesis.task.presentation.DataMapper
import com.github.genesis.task.presentation.feature.search.model.RepositoryItem
import javax.inject.Inject

class RepositoryDataMapper @Inject constructor() : DataMapper<RepositoryItem, Repository> {

    override fun mapToObject(source: Repository): RepositoryItem =
        with(source) {
            RepositoryItem(
                stargazersCount = stargazersCount,
                name = name,
                htmlUrl = htmlUrl,
                description = description,
                id = id
            )
        }
}
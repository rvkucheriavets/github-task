package com.github.genesis.task.presentation.feature.login.di.module

import android.arch.lifecycle.ViewModel
import com.github.genesis.task.presentation.feature.login.viewmodel.LoginViewModel
import com.github.genesis.task.presentation.injection.scopes.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class LoginActivityModule {

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel
}

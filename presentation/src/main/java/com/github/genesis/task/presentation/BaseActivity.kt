package com.github.genesis.task.presentation

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.jakewharton.rxbinding2.view.RxView
import dagger.android.AndroidInjection
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.view_toolbar.*

import javax.inject.Inject

abstract class BaseActivity<V : BaseViewModel> : AppCompatActivity(), ErrorDialogListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: V

    private var errorDialog: MaterialDialog? = null

    protected val disposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        initViewModel()

        initLiveData()

        baseActionLiveData()

        initActionLiveEvent()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
    }

    override fun showErrorDialog(title: String, message: String, action: () -> Unit?) {
        errorDialog = MaterialDialog.Builder(this)
            .title(title)
            .content(message)
            .positiveText(R.string.button_ok)
            .onPositive { dialog, _ -> action.invoke(); dialog.dismiss() }
            .cancelListener { action.invoke() }
            .dismissListener { action.invoke() }
            .build()
        errorDialog?.show()
    }

    abstract fun initViewModel()

    abstract fun initActionLiveEvent()

    abstract fun initLiveData()

    open fun initListeners() {
        arrowBackView?.setOnClickListener { onBackPressed() }
    }

    override fun onBackPressed() {
        finishAndRemoveTask()
    }

    protected fun setViewModel(klass: Class<V>) {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(klass)
    }

    protected fun Disposable.addToCompositeDisposable() {
        disposable.add(this)
    }

    private fun baseActionLiveData() {
        viewModel.errorLiveEvent.observe(this, Observer {
            showErrorDialog(
                title = getString(R.string.dialog_error_title_default),
                message = getString(R.string.dialog_error_message_default)
            )
        })
    }

    companion object {
        const val MIN_CLICK_INTERVAL = 500L
    }
}
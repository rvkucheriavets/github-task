package com.github.genesis.task.presentation.feature.history

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.github.genesis.task.presentation.BaseActivity
import com.github.genesis.task.presentation.R
import com.github.genesis.task.presentation.feature.history.adapter.SearchHistoryAdapter
import com.github.genesis.task.presentation.feature.history.viewmodel.SearchHistoryViewModel
import com.github.genesis.task.presentation.feature.search.SearchActivity
import kotlinx.android.synthetic.main.activity_search_history.*
import kotlinx.android.synthetic.main.view_toolbar.*
import javax.inject.Inject

class SearchHistoryActivity : BaseActivity<SearchHistoryViewModel>() {

    @Inject
    lateinit var adapter: SearchHistoryAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_history)
        initListeners()
        initAdapter()
        toolbarTitle?.text = getString(R.string.activity_history_title)
    }

    override fun initViewModel() {
        setViewModel(SearchHistoryViewModel::class.java)
    }

    override fun initActionLiveEvent() {
        viewModel.openSearchResults.observe(this, Observer {
            startActivity(SearchActivity.getIntent(this, false, it))
        })
    }

    override fun initLiveData() {
        viewModel.latestSearchQueryLiveData.observe(this, Observer {
            it?.let { adapter.setItems(it) }
        })
    }

    private fun initAdapter() {
        with(searchHistoryListView) {
            adapter = this@SearchHistoryActivity.adapter
            layoutManager = LinearLayoutManager(this@SearchHistoryActivity)
        }
    }

    companion object {

        fun getIntent(context: Context) = Intent(context, SearchHistoryActivity::class.java)
    }
}
package com.github.genesis.task.presentation.common

import com.github.genesis.task.domain.feature.search.model.Repository
import com.github.genesis.task.presentation.feature.search.model.SearchQueryData

sealed class Click {

    object RepositoryEditStateChange : Click()

    data class RepositoryItemClick(val item: Repository?) : Click()
    data class SearchHistoryItemClick(val item: SearchQueryData?) : Click()
}
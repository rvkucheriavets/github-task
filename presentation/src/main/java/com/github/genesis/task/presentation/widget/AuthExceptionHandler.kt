package com.github.genesis.task.presentation.widget

/**
 * @author r_kucheriavets
 */
interface AuthExceptionHandler {

    fun startLoginActivity()

    fun startForcedLogoutActivity()

    fun sendFeedback()

}
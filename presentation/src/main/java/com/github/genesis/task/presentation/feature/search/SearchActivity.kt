package com.github.genesis.task.presentation.feature.search

import android.arch.lifecycle.Observer
import android.arch.paging.PagedList
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.github.genesis.task.domain.feature.search.model.Repository
import com.github.genesis.task.presentation.BaseActivity
import com.github.genesis.task.presentation.R
import com.github.genesis.task.presentation.common.SourceState
import com.github.genesis.task.presentation.common.SourceStateHolder
import com.github.genesis.task.presentation.extension.hide
import com.github.genesis.task.presentation.extension.hideKeyboard
import com.github.genesis.task.presentation.extension.toggleView
import com.github.genesis.task.presentation.feature.search.adapter.RepositoriesAdapter
import com.github.genesis.task.presentation.feature.search.model.SearchQueryData
import com.github.genesis.task.presentation.feature.search.model.SortDirectionData
import com.github.genesis.task.presentation.feature.search.model.SortTypeData
import com.github.genesis.task.presentation.feature.search.viewmodel.SearchViewModel
import com.mancj.materialsearchbar.MaterialSearchBar
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.view_loading.*
import kotlinx.android.synthetic.main.view_toolbar.*
import javax.inject.Inject

class SearchActivity : BaseActivity<SearchViewModel>(), MaterialSearchBar.OnSearchActionListener {

    @Inject
    lateinit var adapter: RepositoriesAdapter

    private var menu: Menu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        initListeners()
        initAdapter()
        toolbarTitle.text = getString(R.string.activity_search_title)
        if (intent.getBooleanExtra(EXTRA_IS_SEARCH_BAR_ENABLED, true)) {
            initSearchBar()
        } else {
            searchBarView.hide()
            viewModel.searchForRepositories(intent.getParcelableExtra(EXTRA_SEARCH_QUERY))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        this.menu = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.menu_delete -> {

                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun initViewModel() {
        setViewModel(SearchViewModel::class.java)
    }

    override fun initActionLiveEvent() {
        viewModel.openRepositoryUrl.observe(this, Observer {
            it?.let {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(it)))
            }
        })
    }

    override fun initLiveData() {
        with(viewModel) {
            pagedListLiveData.observe(this@SearchActivity, Observer {
                it?.let { handleSourceState(it) }
            })

            latestSearchQueryLiveData.observe(this@SearchActivity, Observer {
                it?.let { searchBarView?.lastSuggestions = it }
            })

            editStateLiveData.observe(this@SearchActivity, Observer {
                it?.let { updateAdapterEditState(it) }
            })
        }
    }

    override fun onButtonClicked(buttonCode: Int) {
        if (buttonCode == MaterialSearchBar.BUTTON_BACK) {
            searchBarView.disableSearch()
        }
    }

    override fun onSearchStateChanged(enabled: Boolean) {
        if (!enabled) {
            adapter.currentList?.detach()
        }
    }

    override fun onSearchConfirmed(text: CharSequence?) {
        hideKeyboard()
        viewModel.searchForRepositories(SearchQueryData(text.toString(), SortTypeData.STARS, SortDirectionData.DESC))
    }

    private fun updateAdapterEditState(isEditable: Boolean) {
        menu?.findItem(R.id.menu_delete)?.let {
            it.isVisible = isEditable
        }
    }

    private fun initSearchBar() {
        with(searchBarView) {
            setOnSearchActionListener(this@SearchActivity)
            inflateMenu(R.menu.search_menu)
            setCardViewElevation(10)
        }
    }

    private fun initAdapter() {
        with(searchResultListView) {
            adapter = this@SearchActivity.adapter
            layoutManager = LinearLayoutManager(this@SearchActivity)
        }
    }

    private fun handleSourceState(sourceWithState: SourceStateHolder<PagedList<Repository>>) {
        with(sourceWithState) {
            when (status) {
                SourceState.SUCCESS -> onSuccessSearchResult(data)
                SourceState.ERROR -> onErrorSearchResult(message
                    ?: getString(R.string.dialog_error_message_default))
                SourceState.LOADING -> loadingView?.toggleView(adapter.itemCount == 0)
            }
        }
    }

    private fun onSuccessSearchResult(list: PagedList<Repository>?) {
        loadingView?.hide()
        adapter.submitList(list)
    }

    private fun onErrorSearchResult(message: String) {
        loadingView?.hide()
        showErrorDialog("Search error", message)
    }

    companion object {

        private const val EXTRA_IS_SEARCH_BAR_ENABLED = "EXTRA_IS_SEARCH_BAR_ENABLED"
        private const val EXTRA_SEARCH_QUERY = "EXTRA_SEARCH_QUERY"

        fun getIntent(context: Context, isSearchBarEnabled: Boolean = true, query: SearchQueryData? = null) =
            Intent(context, SearchActivity::class.java).apply {
                putExtra(EXTRA_IS_SEARCH_BAR_ENABLED, isSearchBarEnabled)
                putExtra(EXTRA_SEARCH_QUERY, query)
            }
    }
}
package com.github.genesis.task.presentation


interface ErrorDialogListener {

    fun showErrorDialog(title: String, message: String, action: () -> Unit? = {})
}
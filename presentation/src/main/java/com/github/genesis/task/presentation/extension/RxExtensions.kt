package com.github.genesis.task.presentation.extension

import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable
import io.reactivex.functions.Consumer

inline fun <T> Relay<T>.asConsumer(): Consumer<T> {
    return this
}

inline fun <T> Relay<T>.asObservable(): Observable<T> {
    return this.hide()
}
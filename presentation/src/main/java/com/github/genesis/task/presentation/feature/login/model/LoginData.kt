package com.github.genesis.task.presentation.feature.login.model

data class LoginData(
    val login: String = "",
    val password: String = ""
)
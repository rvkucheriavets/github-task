package com.github.genesis.task.presentation.injection.module

import com.github.genesis.task.data.feature.login.datasource.UserCacheDataStore
import com.github.genesis.task.data.feature.login.datasource.UserRemoteDataStore
import com.github.genesis.task.data.feature.login.mapper.LoginEntityMapper
import com.github.genesis.task.data.feature.login.repository.UserDataRepository
import com.github.genesis.task.data.feature.search.datasource.RepositoriesDataSourceFactory
import com.github.genesis.task.data.feature.search.mapper.RepositoryEntityMapper
import com.github.genesis.task.data.feature.search.repository.SearchDataRepository
import com.github.genesis.task.domain.feature.login.repository.UserRepository
import com.github.genesis.task.domain.feature.search.repository.SearchRepository
import com.github.genesis.task.presentation.injection.scopes.ApplicationScope
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@Module
class RepositoryModule {

    @Provides
    @ApplicationScope
    internal fun provideUserRepository(
        cacheDataSource: UserCacheDataStore,
        remoteDataStore: UserRemoteDataStore,
        loginEntityMapper: LoginEntityMapper
    ): UserRepository = UserDataRepository(
        cacheDataSource,
        remoteDataStore,
        loginEntityMapper
    )

    @Provides
    @ApplicationScope
    internal fun provideSearchRepository(
        repositoriesDataSourceFactory: RepositoriesDataSourceFactory,
        repositoryMapper: RepositoryEntityMapper,
        disposable: CompositeDisposable
    ): SearchRepository = SearchDataRepository(
        repositoriesDataSourceFactory,
        repositoryMapper,
        disposable
    )
}
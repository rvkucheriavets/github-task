package com.github.genesis.task.presentation

import android.arch.lifecycle.ViewModel
import android.os.Handler
import android.os.Looper
import android.os.RemoteException
import android.util.Log
import com.github.genesis.task.presentation.common.SingleLiveEvent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.exceptions.CompositeException
import io.reactivex.plugins.RxJavaPlugins

abstract class BaseViewModel : ViewModel() {

    val disposable = CompositeDisposable()

    val errorLiveEvent = SingleLiveEvent<Unit>()

    private val mainHandler = Handler(Looper.getMainLooper())

    init {
        RxJavaPlugins.setErrorHandler {
            Log.e(it::class.java.simpleName, it.message, it)
            onErrorHandler(it)
        }
    }

    override fun onCleared() {
        disposable.clear()
        mainHandler.removeCallbacks(null)
        super.onCleared()
    }

    protected fun onErrorHandler(throwable: Throwable, action: () -> Unit = {}) {
        fun handleException(throwable: Throwable) {
            Log.e(throwable::class.java.simpleName, throwable.message, throwable)
            when (throwable) {
                is RemoteException -> action.invoke()
                else -> {
                    action.invoke()
                }
            }
        }

        if (throwable is CompositeException) {
            throwable.exceptions.forEach {
                handleException(it)
            }
        } else {
            handleException(throwable)
        }
    }
}
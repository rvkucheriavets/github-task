package com.github.genesis.task.presentation.injection.module

import com.github.genesis.task.data.EntityMapper
import com.github.genesis.task.data.cache.CacheMapper
import com.github.genesis.task.data.cache.feature.search.request.mapper.CachedSearchQueryMapper
import com.github.genesis.task.data.cache.feature.search.request.model.CachedSearchQuery
import com.github.genesis.task.data.cache.feature.search.result.mapper.CachedOwnerMapper
import com.github.genesis.task.data.cache.feature.search.result.mapper.CachedRepositoryMapper
import com.github.genesis.task.data.cache.feature.search.result.model.CachedOwner
import com.github.genesis.task.data.cache.feature.search.result.model.CachedRepository
import com.github.genesis.task.data.feature.search.entity.OwnerEntity
import com.github.genesis.task.data.feature.search.entity.RepositoryEntity
import com.github.genesis.task.data.feature.search.mapper.OwnerEntityMapper
import com.github.genesis.task.data.remote.ModelMapper
import com.github.genesis.task.data.remote.feature.search.mapper.OwnerModelMapper
import com.github.genesis.task.data.remote.feature.search.model.OwnerModel
import com.github.genesis.task.domain.feature.search.model.Owner
import com.github.genesis.task.domain.feature.search.model.SearchQuery
import com.github.genesis.task.presentation.DataMapper
import com.github.genesis.task.presentation.feature.search.mapper.SearchQueryDataMapper
import com.github.genesis.task.presentation.feature.search.mapper.SearchQueryMapper
import com.github.genesis.task.presentation.feature.search.model.SearchQueryData
import com.github.genesis.task.presentation.injection.scopes.ApplicationScope
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class MapperModule {

    @Binds
    abstract fun bindOwnerEntityMapper(mapper: OwnerEntityMapper): EntityMapper<OwnerEntity, Owner>

    @Binds
    abstract fun bindOwnerModelMapper(mapper: OwnerModelMapper): ModelMapper<OwnerModel, OwnerEntity>

    @Binds
    abstract fun bindSearchQueryMapper(mapper: SearchQueryMapper): DataMapper<SearchQuery, SearchQueryData>

    @Binds
    abstract fun bindSearchQueryDataMapper(mapper: SearchQueryDataMapper): DataMapper<SearchQueryData, SearchQuery>

    @Binds
    abstract fun bindCachedSearchQueryMapper(mapper: CachedSearchQueryMapper): CacheMapper<CachedSearchQuery, SearchQuery>

    @Binds
    abstract fun bindCachedOwnerMapper(mapper: CachedOwnerMapper): CacheMapper<CachedOwner, OwnerEntity>

    @Binds
    abstract fun bindCachedRepositoryMapper(mapper: CachedRepositoryMapper): CacheMapper<CachedRepository, RepositoryEntity>
}
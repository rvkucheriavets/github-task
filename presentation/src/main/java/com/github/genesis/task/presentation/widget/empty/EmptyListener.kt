package com.github.genesis.task.presentation.widget.empty

interface EmptyListener {

    fun onCheckAgainClicked()

}
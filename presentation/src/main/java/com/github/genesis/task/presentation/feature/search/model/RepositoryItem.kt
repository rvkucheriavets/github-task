package com.github.genesis.task.presentation.feature.search.model

data class RepositoryItem(
    val stargazersCount: Int,
    val description: String,
    val htmlUrl: String,
    val name: String,
    val id: Long
)
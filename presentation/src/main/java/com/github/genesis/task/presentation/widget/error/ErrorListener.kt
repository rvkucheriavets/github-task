package com.github.genesis.task.presentation.widget.error

interface ErrorListener {

    fun onTryAgainClicked()

}
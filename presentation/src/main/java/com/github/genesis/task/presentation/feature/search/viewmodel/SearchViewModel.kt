package com.github.genesis.task.presentation.feature.search.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PagedList
import com.github.genesis.task.domain.feature.search.model.Repository
import com.github.genesis.task.domain.feature.search.usecase.LatestSearchQueriesUseCase
import com.github.genesis.task.domain.feature.search.usecase.SearchUseCase
import com.github.genesis.task.domain.feature.search.usecase.UpdateViewedStateUseCase
import com.github.genesis.task.presentation.BaseViewModel
import com.github.genesis.task.presentation.common.Click
import com.github.genesis.task.presentation.common.SingleLiveEvent
import com.github.genesis.task.presentation.common.SourceStateHolder
import com.github.genesis.task.presentation.feature.search.mapper.SearchQueryDataMapper
import com.github.genesis.task.presentation.feature.search.mapper.SearchQueryMapper
import com.github.genesis.task.presentation.feature.search.model.SearchQueryData
import com.github.genesis.task.presentation.util.RxBus
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class SearchViewModel @Inject constructor(
    private val searchUseCase: SearchUseCase,
    private val updateViewedStateUseCase: UpdateViewedStateUseCase,
    private val searchQueryMapper: SearchQueryMapper,
    private val searchQueryDataMapper: SearchQueryDataMapper,
    latestSearchQueriesUseCase: LatestSearchQueriesUseCase,
    bus: RxBus
) : BaseViewModel() {

    val pagedListLiveData = MutableLiveData<SourceStateHolder<PagedList<Repository>>>()

    val latestSearchQueryLiveData = MutableLiveData<List<String>>()

    val openRepositoryUrl = SingleLiveEvent<String>()

    val editStateLiveData = MutableLiveData<Boolean>()

    init {
        editStateLiveData.value = false

        latestSearchQueriesUseCase.execute()
            .map { it.map { searchQueryDataMapper.mapToObject(it).query } }
            .subscribeBy { latestSearchQueryLiveData.postValue(it) }
            .addTo(disposable)

        bus.clicksObservable
            .filter { it is Click }
            .map { it as Click }
            .doOnNext { handleClick(it) }
            .subscribe()
            .addTo(disposable)
    }

    // TODO at this moment pagedList not supports type transforming, so we have to wait to fix this cause this issue already on the issue tracker of arch components project
    fun searchForRepositories(searchQuery: SearchQueryData?) {
        searchQuery?.let { query ->
            searchUseCase.execute(params = searchQueryMapper.mapToObject(query))
                .doOnSubscribe { pagedListLiveData.postValue(SourceStateHolder.loading()) }
                .subscribe(
                    {
                        pagedListLiveData.postValue(SourceStateHolder.success(it))
                    },
                    {
                        onErrorHandler(it) {
                            pagedListLiveData.postValue(SourceStateHolder.error(it.message))
                        }
                    }
                )
                .addTo(disposable)
        }
    }

    private fun updateViewedState(repository: Repository?) {
        repository?.let {
            updateViewedStateUseCase.execute(repository.id to true)
                .subscribe()
                .addTo(disposable)
        }
    }

    private fun handleClick(click: Click) {
        when (click) {
            is Click.RepositoryItemClick -> {
                // TODO Need to fix issue with update current pagedList items
                updateViewedState(click.item)
                openRepositoryUrl.value = click.item?.htmlUrl
            }

            is Click.RepositoryEditStateChange -> {
                editStateLiveData.value?.let {
                    editStateLiveData.postValue(!it)
                }
            }
        }
    }
}
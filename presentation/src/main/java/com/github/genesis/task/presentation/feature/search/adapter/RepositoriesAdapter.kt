package com.github.genesis.task.presentation.feature.search.adapter

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.genesis.task.domain.feature.search.model.Repository
import com.github.genesis.task.presentation.R
import com.github.genesis.task.presentation.common.Click
import com.github.genesis.task.presentation.extension.toggleView
import com.github.genesis.task.presentation.util.RxBus
import kotlinx.android.synthetic.main.item_repository.view.*
import javax.inject.Inject

class RepositoriesAdapter @Inject constructor(
    private val bus: RxBus
) : PagedListAdapter<Repository, RepositoriesAdapter.ViewHolder>(diffCallback), ItemTouchHelperAdapter {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_repository, parent, false)
        return ViewHolder(view).apply {
            with(view) {
                setOnClickListener {
                    bus.clicksConsumer.accept(Click.RepositoryItemClick(getItem(adapterPosition)))
                }

                setOnLongClickListener {
                    bus.clicksConsumer.accept(Click.RepositoryEditStateChange)
                    return@setOnLongClickListener true
                }
            }

        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let {
            holder.setData(it)
        }
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int) {
        // PagedList not support this kind of job if you don't use room behavior
    }

    override fun onItemDismiss(position: Int) {
        // PagedList not support this kind of job if you don't use room behavior
    }

    inner class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun setData(data: Repository) {
            with(view) {
                repositoryTitleView.text = data.name
                repositoryDescriptionView.text = data.description
                repositoryStarCountView.text = data.stargazersCount.toString()
                viewedStateImageView.toggleView(data.isViewed)
            }
        }
    }

    companion object {

        val diffCallback = object : DiffUtil.ItemCallback<Repository>() {

            override fun areItemsTheSame(oldItem: Repository?, newItem: Repository?): Boolean {
                return oldItem?.id == newItem?.id
            }

            override fun areContentsTheSame(oldItem: Repository?, newItem: Repository?): Boolean {
                return oldItem == newItem
            }
        }
    }
}
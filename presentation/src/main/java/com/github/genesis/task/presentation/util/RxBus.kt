package com.github.genesis.task.presentation.util

import com.github.genesis.task.presentation.extension.asConsumer
import com.github.genesis.task.presentation.extension.asObservable
import com.jakewharton.rxrelay2.PublishRelay
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RxBus @Inject constructor() {

    private val clickRelay: PublishRelay<Any> = PublishRelay.create()

    private val eventsRelay: PublishRelay<Any> = PublishRelay.create()

    val clicksConsumer get() = clickRelay.asConsumer()

    val clicksObservable get() = clickRelay.asObservable()

    val eventsConsumer get() = eventsRelay.asConsumer()

    val eventsObservable get() = eventsRelay.asObservable()
}